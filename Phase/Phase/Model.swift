//
//  Model.swift
//  Phase
//
//  Created by Mahabir on 07/03/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import Foundation


class Post: NSObject {
    
    var postTitle:String!
    var lat:String!
    var long:String!
    var _id:String!
    var createdDate:String!
    var type:String!
    var fileType:String!
    var userName:String!
    var userId:String!
    var picturePath:String!
    
    init(postTitle:String,lat:String,long:String,_id:String,createdDate:String,type:String,fileType:String,userName:String,userId:String,picturePath:String) {
        self.postTitle = postTitle
        self.lat = lat
        self.long = long
        self._id = _id
        self.createdDate = createdDate
        self.type = type
        self.fileType = fileType
        self.userName = userName
        self.userId = userId
        self.picturePath = picturePath
    }
    
}

class Request:NSObject {
    
    var type:String!
    var name:String!
    var friendId:String!
    var requestId:String!
    var status:String!
    
    init(type:String,name:String,friendId:String,requestId:String,status:String!) {
        self.type = type
        self.friendId = friendId
        self.name = name
        self.requestId = requestId
        self.status = status
    }
    
}
