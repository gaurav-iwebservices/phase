//
//  StatusCell2.swift
//  Phase
//
//  Created by Mahabir on 16/02/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit

class StatusCell2: UITableViewCell {
    
    @IBOutlet weak var dropDownBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var updateImageView: UIImageView!
     @IBOutlet weak var backgroundContentView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
       
        self.backgroundContentView.layer.cornerRadius = 5
        self.backgroundContentView.layer.borderWidth = 1
        self.backgroundContentView.layer.borderColor = UIColor(red: 222.0/255.0, green: 222.0/255.0, blue: 222.0/255.0, alpha: 1.0).cgColor
        self.backgroundContentView.layer.masksToBounds = true
        self.updateImageView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
