//
//  ChatHeaderCell.swift
//  Phase
//
//  Created by Mahabir on 06/03/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit

class ChatHeaderCell: UITableViewCell {

    @IBOutlet weak var headerLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
