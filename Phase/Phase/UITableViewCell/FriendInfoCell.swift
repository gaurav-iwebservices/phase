//
//  FriendInfoCell.swift
//  Phase
//
//  Created by Mahabir on 23/03/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit

class FriendInfoCell: UITableViewCell {

    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var photos: UIButton!
    @IBOutlet weak var educationLabel: UILabel!
    @IBOutlet weak var professionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
