//
//  SettingCell.swift
//  Phase
//
//  Created by Mahabir on 16/02/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit

class SettingCell: UITableViewCell {
    @IBOutlet weak var menuImageView: UIImageView!
    @IBOutlet weak var menuNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
