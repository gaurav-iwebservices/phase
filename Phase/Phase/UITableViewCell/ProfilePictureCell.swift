//
//  ProfilePictureCell.swift
//  Phase
//
//  Created by Mahabir on 26/02/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit

class ProfilePictureCell: UITableViewCell {
    @IBOutlet weak var changeProfilePicBtn: DesignButton!
    @IBOutlet weak var changeCoverPicBtn: UIButton!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profiePicImage: UIImageView!
    @IBOutlet weak var coverImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        profiePicImage.layer.borderWidth = 2
        profiePicImage.layer.borderColor = UIColor.white.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
