//
//  CommentCell.swift
//  Phase
//
//  Created by Mahabir on 05/03/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {
    @IBOutlet weak var commentorImage: UIImageView!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var commentorNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commentorImage.layer.cornerRadius = commentorImage.frame.size.height/2
        commentorImage.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
