//
//  StatusCell3.swift
//  Phase
//
//  Created by Mahabir on 14/03/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit
import AVFoundation
import NotificationCenter


private var playerViewControllerKVOContext = 0
class StatusCell3: UITableViewCell {
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var dropDownBtn: UIButton!
    
    
    var player : AVPlayer!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var timeSlider: UISlider!
    @IBOutlet weak var totalTimeLabel: UILabel!
    
    ///////
    
    private var timeObserverToken: Any?
    /*
     A formatter for individual date components used to provide an appropriate
     value for the `startTimeLabel` and `durationLabel`.
     */
    let timeRemainingFormatter: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.zeroFormattingBehavior = .pad
        formatter.allowedUnits = [.minute, .second]
        
        return formatter
    }()
    
    var currentTime1: Double {
        get {
            return CMTimeGetSeconds(player.currentTime())
        }
        set {
            let newTime = CMTimeMakeWithSeconds(newValue, 1)
            player.seek(to: newTime, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
        }
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backView.layer.cornerRadius = 5
        self.backView.layer.borderWidth = 1
        self.backView.layer.borderColor = UIColor(red: 222.0/255.0, green: 222.0/255.0, blue: 222.0/255.0, alpha: 1.0).cgColor
        playBtn.addTarget(self, action: #selector(self.playAction(sender:)), for: UIControlEvents.touchUpInside)
   
       // timeSlider.addTarget(self, action: #selector(self.handlePlayheadSliderTouchBegin), for: .touchDown)
       // timeSlider.addTarget(self, action:    #selector(self.handlePlayheadSliderTouchEnd), for: .touchUpInside)
       // timeSlider.addTarget(self, action: #selector(self.handlePlayheadSliderTouchEnd), for: .touchUpOutside)
        timeSlider.addTarget(self, action: #selector(self.handlePlayheadSliderValueChanged), for: .valueChanged)
    
    }
    
    @objc func playAction(sender:UIButton) {
        if sender.titleLabel?.text == "Play" {
            sender.setTitle("Pause", for: UIControlState.normal)
            player.play()
        }else{
            sender.setTitle("Play", for: UIControlState.normal)
            player.pause()
        }
    }
    
    func video(url:URL) {
        
        player = AVPlayer(url:url)
        player.rate = 1
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.videoView.bounds
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        self.videoView.layer.insertSublayer(playerLayer, at: 0)
        
        let duration : CMTime = player.currentItem!.asset.duration
        let seconds : Float64 = CMTimeGetSeconds(duration)
        totalTimeLabel.text = self.stringFromTimeInterval(interval: seconds)
        
        let duration1 : CMTime = player.currentTime()
        let seconds1 : Float64 = CMTimeGetSeconds(duration1)
        startTimeLabel.text = self.stringFromTimeInterval(interval: seconds1)
        timeSlider.maximumValue = Float(seconds)
        
        let interval = CMTimeMake(1, 1)
        timeObserverToken = player.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main) { [unowned self] time in
            let timeElapsed = Float(CMTimeGetSeconds(time))

            self.timeSlider.value = Float(timeElapsed)
            self.startTimeLabel.text = self.createTimeString(time: timeElapsed)
        }
        
    }
    //MARK:// Slider Action
    
    @IBAction func handlePlayheadSliderTouchBegin(_ sender: UISlider) {
        player.pause()
    }
    
    @IBAction func handlePlayheadSliderValueChanged(_ sender: UISlider) {
        
//        let duration : CMTime = player.currentItem!.asset.duration
//        let seconds : Float64 = CMTimeGetSeconds(duration) * Float64(sender.value)
//        //   var newCurrentTime: TimeInterval = sender.value * CMTimeGetSeconds(currentPlayer.currentItem.duration)
//        startTimeLabel.text = self.stringFromTimeInterval(interval: seconds)
        
        currentTime1 = Double(sender.value)
        if playBtn.titleLabel?.text == "Play" {
            player.play()
        }else{
            player.pause()
        }
    }
    
//    @IBAction func handlePlayheadSliderTouchEnd(_ sender: UISlider) {
//
////        let duration : CMTime = player.currentItem!.asset.duration
////        let newCurrentTime: TimeInterval = Float64(sender.value) * CMTimeGetSeconds(duration)
////        let seekToTime: CMTime = CMTimeMakeWithSeconds(newCurrentTime, 600)
////      //  player.seek(toTime: seekToTime)
////        player.seek(to: seekToTime)
//    }
//
    func stringFromTimeInterval(interval: TimeInterval) -> String {
        let interval = Int(interval)
        let seconds = interval % 60
        let minutes = (interval / 60) % 60
       // let hours = (interval / 3600)
        //return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
        return String(format: "%02d:%02d", minutes, seconds)
    }
   
    // MARK: Convenience
    
    func createTimeString(time: Float) -> String {
        let components = NSDateComponents()
        components.second = Int(max(0.0, time))
        return timeRemainingFormatter.string(from: components as DateComponents)!
    }
    
   

}
