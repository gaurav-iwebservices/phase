//
//  ProfileInfoCell.swift
//  Phase
//
//  Created by Mahabir on 26/02/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit

class ProfileInfoCell: UITableViewCell {
    @IBOutlet weak var describeText: UITextField!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var educationLabel: UILabel!
    @IBOutlet weak var professionLabel: UILabel!
    @IBOutlet weak var editDetailBtn: UIButton!
    
    @IBOutlet weak var photos: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        describeText.layer.borderWidth = 1
        describeText.layer.cornerRadius = 5
        describeText.layer.borderColor = UIColor.black.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
