//
//  Defines.swift
//  NicoBeacon
//
//  Created by Vinay on 21/08/17.
//  Copyright © 2017 iWeb. All rights reserved.
//

import UIKit

class Defines: NSObject
{
    // MARK: Message
    
    
    static let kStartColor = UIColor(red: 179.0/255.0, green: 18.0/255.0, blue: 23.0/255.0, alpha: 1.0).cgColor
    static let kEndColor = UIColor(red: 246.0/255.0, green: 106.0/255.0, blue: 101.0/255.0, alpha: 1.0).cgColor
    
     static let kTextViewUnderLineColor = UIColor.gray.cgColor
    
    static let alterTitle = "Alert!"
    static let successTitle = "Success!"
    static let invalidEmail = "Please fill valid email address."
    static let password = "Please fill valid password."
    static let loginErrorMessage = "Please check your internet connection"
    static let underDevelopmentMessageMessage = "Underdevelopment"
    static let noRecordFound = "No Record Found!"
    static let noInterNet = "Please check your internet connection"
    static let blankUserName = "Please fill Username!"


    static let networkErrorMessage = "Please check your internet connection"
    static let deleteDeviceAlertMessage = "Are you sure to remove this device?"
    
    // MARK: API
    
    static let ServerUrl = "http://67.205.173.26:3000/api/"
    static let register = "users/signup"
    static let signIn = "users/signin"
    static let verifyOtp = "users/verifyotp"
    static let resendOTP = "users/resendotp"
    static let forgotPassword = "users/forgetpassword"
    static let countryList = "users/countryList"
    static let addPost = "posts/addPost/"
    static let getPost = "posts/getUserPost/"
    static let search = "users/search"
    static let addEvent = "posts/addEvent"
    static let addComment = "posts/commentPost"
    static let deletePost = "posts/deletepost/"
    static let friendRequest = "users/receiveFriendRequestList/"
    static let sendFriendRequest = "users/sendfriendrequest"
    static let changeFriendStatus = "users/changeFriendStatus"
    static let getAroundPost = "posts/getAroundPost"
    static let socialLogin = "users/signupSocial"
    static let like = "posts/likePost"
    static let getProfile = "users/userProfile/"
    static let changeProfilePicture = "users/changeProfilePicture"
    static let changeCoverPicture = "users/changeCoverPicture"
    static let editProfile =  "users/editProfile"
    static let describeMe = "users/describeme"
    static let gallery = "users/getUserGallery/"
    

}
