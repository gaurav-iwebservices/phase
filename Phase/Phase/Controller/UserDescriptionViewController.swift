//
//  UserDescriptionViewController.swift
//  Phase
//
//  Created by Mahabir on 05/04/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit
import NotificationBannerSwift
import NVActivityIndicatorView

class UserDescriptionViewController: UIViewController {

    @IBOutlet weak var descriptionTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    
    @IBAction func saveAction(_ sender: UIButton) {
        self.saveDescription()
    }
    
    
    func saveDescription(){
        
        let parameter:[String:String] = [
            "user_id":UserData._id,
            "describeme":descriptionTextView.text!
        ]
        
        if  !Reachability.isConnectedToNetwork() {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.noInterNet)
            return
        }
        
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        
        DataProvider.sharedInstance.getDataUsingPost(path: Defines.ServerUrl + Defines.describeMe, dataDict: parameter, { (json) in
            print(json)
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            
            if json["status"].stringValue == "true" {
                let banner = NotificationBanner(title: "Confirmation", subtitle: json["msg"].stringValue , style: .success)
                banner.show(queuePosition: .front)
                
            }else if json["status"].stringValue == "false" {
                let banner = NotificationBanner(title: "Alert", subtitle: json["msg"].stringValue , style: .danger)
                banner.show(queuePosition: .front)
            }
            
        }) { (error) in
            print(error)
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
