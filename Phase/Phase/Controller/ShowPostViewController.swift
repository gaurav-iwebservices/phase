//
//  ShowPostViewController.swift
//  Phase
//
//  Created by Mahabir on 28/02/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit
import AlamofireImage

class ShowPostViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var commentTextView: UITextView!
    
    var postId:String!
    var statusText:String!
    var imageUrl:String!
    var postType:String!
    var userName:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.backgroundView.layer.cornerRadius = 5
        self.backgroundView.layer.borderColor = UIColor(red: 222.0/255.0, green: 222.0/255.0, blue: 222.0/255.0, alpha: 1.0).cgColor
        self.backgroundView.layer.borderWidth = 1
        tableView.separatorStyle = .none
        
        commentTextView.layer.cornerRadius = commentTextView.frame.size.height/2
        commentTextView.layer.borderWidth = 1
        commentTextView.layer.borderColor = UIColor.gray.cgColor
        commentTextView.tintColor = UIColor.black
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @IBAction func sendAction(_ sender: UIButton) {
        self.addComment()
    }
    
    func addComment() {
        
        if  !Reachability.isConnectedToNetwork() {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.noInterNet, withError: nil, onClose: {
                return
            })
        }
        
        let parameter =  [
            "user_id":UserData.userToken,
            "post_id":postId,
            "status":"",
            "comment_text":commentTextView.text!
        ]
        
        DataProvider.sharedInstance.getDataUsingPost(path: Defines.ServerUrl + Defines.addComment, dataDict: parameter, { (json) in
            print(json)
        }) { (error) in
            print(error)
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ShowPostViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            if postType == "0" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "StatusCell", for: indexPath) as! StatusCell
                cell.userNameLabel.text = userName
                cell.statusLabel.text = statusText
                cell.backgroundContentView.layer.borderWidth = 0
                cell.selectionStyle = .none
                return cell
            }else if postType == "1" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "StatusCell2", for: indexPath) as! StatusCell2
                cell.backgroundContentView.layer.borderWidth = 0
                cell.userNameLabel.text = userName
                cell.statusLabel.text = statusText
                let url = URL(string:imageUrl)
                let placeholder = UIImage(named: "default")
                
                let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                    size: cell.updateImageView.frame.size,
                    radius: 20.0
                )
                
                // let filter = AspectScaledToFillSizeFilter(size: cell.updateImageView.frame.size)
                cell.updateImageView.af_setImage(withURL: url! , placeholderImage: placeholder, filter: filter)
                cell.selectionStyle = .none
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "StatusCell2", for: indexPath) as! StatusCell2
                cell.backgroundContentView.layer.borderWidth = 0
                cell.userNameLabel.text = userName
                cell.statusLabel.text = statusText
                cell.selectionStyle = .none
                return cell
            }
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentCell
            cell.selectionStyle = .none
            return cell
        }
    }
}
