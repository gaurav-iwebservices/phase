//
//  OTPViewController.swift
//  Phase
//
//  Created by Mahabir on 22/02/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import NotificationBannerSwift
import NVActivityIndicatorView

class OTPViewController: UIViewController {
    
    @IBOutlet weak var text4: UITextField!
    @IBOutlet weak var text3: UITextField!
    @IBOutlet weak var text2: UITextField!
    @IBOutlet weak var text1: UITextField!
    var userId:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        text1.delegate = self
        text2.delegate = self
        text3.delegate = self
        text4.delegate = self
        
        text1.tintColor = .clear
        text2.tintColor = .clear
        text3.tintColor = .clear
        text4.tintColor = .clear
        
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        text1.becomeFirstResponder()
    }
    
    @IBAction func resendOTPAction(_ sender: UIButton) {
        self.resendOTP()
    }
    
    func resendOTP() {
        let parameter:[String:String] = [
            "": ""
        ]
        
        if  !Reachability.isConnectedToNetwork() {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.noInterNet, withError: nil, onClose: {
                return
            })
        }
        
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        
        DataProvider.sharedInstance.getDataUsingPut(path: Defines.ServerUrl + Defines.resendOTP + "/" + userId, dataDict: parameter, { (json) in
            print(json)
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            if json["status"].stringValue == "true" {
                let banner = NotificationBanner(title: "Confirmation", subtitle: json["msg"].stringValue , style: .success)
                banner.show(queuePosition: .front)
                
            }else if json["status"].stringValue == "false" {
                let banner = NotificationBanner(title: "Alert", subtitle: json["msg"].stringValue , style: .danger)
                banner.show(queuePosition: .front)
            }
        }) { (error) in
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            print(error)
        }
    }
    
    func verifyOTP() {
        
        let parameter:[String:String] = [
            "otp": text1.text! + text2.text! + text3.text! + text4.text!
        ]
        
        if  !Reachability.isConnectedToNetwork() {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.noInterNet, withError: nil, onClose: {
                return
            })
        }
        
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        DataProvider.sharedInstance.getDataUsingPut(path: Defines.ServerUrl + Defines.verifyOtp + "/" + userId, dataDict: parameter, { (json) in
            print(json)
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            if json["status"].stringValue == "true" {
                let banner = NotificationBanner(title: "Confirmation", subtitle: json["msg"].stringValue , style: .success)
                banner.show(queuePosition: .front)
              //  UserDefaults.standard.setValue("yes", forKey: "login")
               /* let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let nextView = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                self.navigationController?.pushViewController(nextView, animated: true)*/
               // self.performSegue(withIdentifier: "register", sender: nil)
                
                let LoginViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let nav = UINavigationController(rootViewController: LoginViewController)
                nav.setNavigationBarHidden(true, animated: false)
                appDelegate.window?.rootViewController = nav
                
            }else if json["status"].stringValue == "false" {
                let banner = NotificationBanner(title: "Alert", subtitle: json["msg"].stringValue , style: .danger)
                banner.show(queuePosition: .front)
            }
        }) { (error) in
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            print(error)
        }
        
    }

   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension OTPViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        let char = string.cString(using: String.Encoding.utf8)
        let isBackSpace: Int = Int(strcmp(char, "\u{8}"))
        if isBackSpace == -8 {
            if textField == text1 {
                    return true
            } else if textField == text2 {
                    textField.text = ""
                    textField.resignFirstResponder()
                    text1.becomeFirstResponder()
            }else if textField == text3 {
                    textField.text = ""
                    textField.resignFirstResponder()
                    text2.becomeFirstResponder()
            }else if textField == text4 {
                textField.text = ""
                textField.resignFirstResponder()
                text3.becomeFirstResponder()
            }
            return false
        }else{
        
            let allowedCharacter = "0123456789"
            let aSet = CharacterSet(charactersIn:allowedCharacter).inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
        if string == numberFiltered{
            
            if textField == text1 {
                /*if textField.text?.count == 0 {
                    textField.text = string
                    textField.resignFirstResponder()
                    text2.becomeFirstResponder()
                }else*/ if textField.text?.count == 1 {
                    textField.resignFirstResponder()
                    text2.text = string
                    text2.becomeFirstResponder()
                }
            } else if textField == text2 {
                /*if textField.text?.count == 0 {
                    textField.text = string
                    textField.resignFirstResponder()
                    text3.becomeFirstResponder()
                }else*/ if textField.text?.count == 1 {
                    textField.resignFirstResponder()
                    text3.text = string
                    text3.becomeFirstResponder()
                }
            }else if textField == text3 {
                /*if textField.text?.count == 0 {
                    textField.text = string
                    textField.resignFirstResponder()
                    text4.becomeFirstResponder()
                }else*/ if textField.text?.count == 1 {
                    textField.resignFirstResponder()
                    text4.text = string
                    text4.becomeFirstResponder()
                    self.verifyOTP()
                }
            }else if textField == text4 {
                
            }
           
            
            let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let numberOfChars = newText.count
            return numberOfChars < 2
            
        }else{
            return string == numberFiltered
        }
        }
    }

    
   
    
}
