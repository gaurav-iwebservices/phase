//
//  Search2ViewController.swift
//  Phase
//
//  Created by Mahabir on 08/03/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import NotificationBannerSwift

class Search2ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var dataArr:[String] = []
    var navigationTitle:String = ""
    var searchType:String!
    var friend:[String] = []
    var peopleId:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = navigationTitle
        
        tableView.separatorStyle = .singleLine
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    
    
    @objc func addFriendAction(sender:UIButton) {
        
        if sender.titleLabel?.text != "add friend" {
            return
        }
        
        let parameter = [
            "send_from_user_id":UserData._id,
            "send_to_user_id":peopleId[sender.tag],
            "type":"1"
        ]
        
        if  !Reachability.isConnectedToNetwork() {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.noInterNet)
            return
        }
        
        print(parameter)
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        DataProvider.sharedInstance.getDataUsingPost(path: Defines.ServerUrl + Defines.sendFriendRequest, dataDict: parameter, { (json) in
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            print(json)
            
            if json["status"].stringValue == "true" {
                let banner = NotificationBanner(title: "Confirmation", subtitle: json["msg"].stringValue , style: .success)
                banner.show(queuePosition: .front)
                sender.setTitle("Request sent", for: UIControlState.normal)
                
            }else if json["status"].stringValue == "false" {
                let banner = NotificationBanner(title: "Alert", subtitle: json["msg"].stringValue , style: .danger)
                banner.show(queuePosition: .front)
            }
            
        }) { (error) in
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            print(error)
        }
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension Search2ViewController : UITableViewDelegate, UITableViewDataSource {
    
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as! SearchCell
        cell.searchDetailLabel.text = dataArr[indexPath.row]
        
        cell.addFriendBtn.addTarget(self, action: #selector(self.addFriendAction(sender:)), for: UIControlEvents.touchUpInside)
        if searchType == "people" {
            cell.addFriendBtn.isHidden = false
            cell.addFriendBtn.tag = indexPath.row
            if friend[indexPath.row] == "1" {
                cell.addFriendBtn.setTitle("friend", for: UIControlState.normal)
            }else if friend[indexPath.row] == "0" {
                cell.addFriendBtn.setTitle("pending", for: UIControlState.normal)
            }else {
                cell.addFriendBtn.setTitle("add friend", for: UIControlState.normal)
            }
            
        }else {
            cell.addFriendBtn.isHidden = true
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let nextView = storyBoard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        nextView.userId = peopleId[indexPath.row]
        self.navigationController?.pushViewController(nextView, animated: true)
    }
    
}
