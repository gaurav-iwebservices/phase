//
//  LoginViewController.swift
//  Phase
//
//  Created by Mahabir on 15/02/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import TwitterKit
import TwitterCore
import NotificationBannerSwift
import NVActivityIndicatorView
import SwiftyJSON
import GoogleSignIn

class LoginViewController: UIViewController, GIDSignInUIDelegate {
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var facebookBtn: DesignButton!
    @IBOutlet weak var twitterBtn: DesignButton!
    @IBOutlet weak var googleBtn: DesignButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        usernameText.delegate = self
        passwordText.delegate = self
        usernameText.tintColor = .white
        passwordText.tintColor = .white
        
        //facebookBtn.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        facebookBtn.setImage(#imageLiteral(resourceName: "facebook"), for: UIControlState.normal)
        twitterBtn.setImage(#imageLiteral(resourceName: "twitter"), for: UIControlState.normal)
        googleBtn.setImage(#imageLiteral(resourceName: "search"), for: UIControlState.normal)
        
        facebookBtn.addTarget(self, action: #selector(self.facebookLoginAction(_:)), for: UIControlEvents.touchUpInside)
        twitterBtn.addTarget(self, action: #selector(self.loginWithTwitter(sender:)), for: UIControlEvents.touchUpInside)
        googleBtn.addTarget(self, action: #selector(self.gmail_touch(_:)), for: UIControlEvents.touchUpInside)
        
        let doubleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard(sender:)))
        self.view.addGestureRecognizer(doubleTapRecognizer)
        GIDSignIn.sharedInstance().uiDelegate = self
        // NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("btnClicked:"), name: "myCustomId", object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.googleAction(notification:)), name: NSNotification.Name(rawValue: "googleSignIn"), object: nil)
    }
    
    @objc func googleAction(notification:NSNotification) {
        let userInfo : [String:String] = (notification.userInfo as? [String:String])!
        print(userInfo)
        UserDefaults.standard.setValue("yes", forKey: "google")
        socialAction(data: userInfo)
    }
    
    @objc func hideKeyboard(sender:UIGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    @IBAction func createOneAction(_ sender: UIButton) {
        
        var isExist = false
        if let viewControllers = self.navigationController?.viewControllers {
            for viewController in viewControllers {
                if viewController.isKind(of: RegisterViewController.self) {
                    isExist = true
                   self.navigationController?.popToViewController(viewController, animated: true)
                }
            }
        }
        
        if isExist == false {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let nextView = storyBoard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
            self.navigationController?.pushViewController(nextView, animated: true)
        }
    }
    
    
    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func forgotPasswordAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let nextView = storyBoard.instantiateViewController(withIdentifier: "ForgotViewController") as! ForgotViewController
        self.navigationController?.pushViewController(nextView, animated: true)
    }
    
    @IBAction func loginAction(_ sender: DesignButton) {
        
        if usernameText.text!.count == 0 {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.blankUserName)
               // self.usernameText.becomeFirstResponder()
                return
        }
        
        if passwordText.text!.count == 0 {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.password)
                //self.passwordText.becomeFirstResponder()
                return
            
        }
        
        if  !Reachability.isConnectedToNetwork() {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.noInterNet)
                return
            
        }
        
        let parameter:[String:String] = [
            "username": usernameText.text!,
            "password":passwordText.text!,
            "device_type":"ios",
            "token_id":"ajskdbfhjlaksvcjhshjdvjahsdcvjhasvclj"
        ]
        
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        DataProvider.sharedInstance.getDataUsingPost(path: Defines.ServerUrl + Defines.signIn, dataDict: parameter, { (json) in
            print(json)
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            if json["status"].stringValue == "true" {
                let banner = NotificationBanner(title: "Confirmation", subtitle: "Login successful." , style: .success)
                banner.show(queuePosition: .front)
                
                UserData._id = json["data"]["_id"].stringValue
                UserData.city = json["data"]["city"].stringValue
                UserData.country = json["data"]["country"].stringValue
                UserData.dob = json["data"]["dob"].stringValue
                UserData.email = json["data"]["email"].stringValue
                UserData.gender = json["data"]["gender"].stringValue
                UserData.mobile = json["data"]["mobile"].stringValue
                UserData.name = json["data"]["first_name"].stringValue
                UserData.userName = json["data"]["username"].stringValue
                UserData.userToken = json["token"].stringValue
                
                UserDefaults.standard.setValue(UserData.name, forKey: "firstName")
                UserDefaults.standard.setValue(UserData.email, forKey: "email")
                UserDefaults.standard.setValue(UserData._id, forKey: "Id")
                UserDefaults.standard.setValue(UserData.city, forKey: "city")
                UserDefaults.standard.setValue(UserData.country, forKey: "country")
                UserDefaults.standard.setValue(UserData.dob, forKey: "dob")
                UserDefaults.standard.setValue(UserData.userName, forKey: "userName")
                UserDefaults.standard.setValue(UserData.gender, forKey: "gender")
                UserDefaults.standard.setValue(UserData.mobile, forKey: "mobile")
                UserDefaults.standard.setValue(UserData.userToken, forKey: "token")
                UserDefaults.standard.setValue("yes", forKey: "login")
                
                
                self.performSegue(withIdentifier: "login", sender: nil)
            }else if json["status"].stringValue == "false" {
                let banner = NotificationBanner(title: "Alert", subtitle: json["msg"].stringValue , style: .danger)
                banner.show(queuePosition: .front)
            }
            
        }) { (error) in
            print(error)
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        }
        
    }
    
    
    
    @IBAction func facebookLoginAction(_ sender: UIButton) {
        
        // loginWithFBbtn.addTarget(self, action: #selector(self.loginButtonClicked), for: UIControlEvents.touchUpInside)
        
        if !Reachability.isConnectedToNetwork() {
            let banner = NotificationBanner(title: "Alert", subtitle: Defines.noInterNet , style: .danger)
            banner.show(queuePosition: .front)
            return
        }
        
        let loginManager = LoginManager()
        
       // if Float(UIDevice.current.systemVersion) ?? 0.0 <= 9 {
         //   loginManager.loginBehavior = LoginBehavior.web
      //  }else{
            loginManager.loginBehavior = LoginBehavior.systemAccount
       // }
        loginManager.logOut()
        loginManager.logIn(readPermissions: [.publicProfile, .email], viewController: self) { (loginResult) in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                
                
                print("Logged in!")
                self.getFBUser()
            }
        }
    }
    
    
    
    func getFBUser() {
        if AccessToken.current != nil {
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"], accessToken: AccessToken.current, httpMethod: .POST, apiVersion: GraphAPIVersion.defaultVersion).start({ (response, result) in
                
                print(response as Any)
                print(result)
                
                switch result {
                case .failed(let error):
                    print("error in graph request:", error)
                    break
                case .success(let graphResponse):
                    if let responseDictionary = graphResponse.dictionaryValue {
                        
                        print(JSON(responseDictionary))
                        let valData = JSON(responseDictionary)
                        /*print(responseDictionary["name"] as! String)
                         print(responseDictionary["email"] as! String)
                         print(responseDictionary["picture"]!["data"]["url"] as! String)
                         print(responseDictionary["id"] as! String)*/
                        
                        
                        //var dict: NSDictionary!
                        
                        //dict = responseDictionary["data"] as! NSDictionary
                        //print(dict)
                        //print(dict["url"])
                        UserDefaults.standard.setValue(valData["picture"]["data"]["url"].stringValue, forKey: "ImgPath")
                        let dataDict:[String:String] =  [
                            "first_name": valData["first_name"].stringValue,
                            "last_name": valData["last_name"].stringValue,
                            "email": valData["email"].stringValue,
                            "username": valData["first_name"].stringValue,
                            "socialkey":"1", // (1=>facebook,2=>twitter ,3=>gmail )
                            "socialid": valData["id"].stringValue
                        ]
                        self.socialAction(data: dataDict)
                        
                    }
                }
            })
        }
    }
    
    @objc func loginWithTwitter(sender:UIButton) {
        TWTRTwitter.sharedInstance().start(withConsumerKey:"mnShAYYEYMlmY4lzr5esbfJF0", consumerSecret:"JGdYIPPtSe9EWSvWqDpUlNsxdHvSyXnbYw47bzHUYYsAiSFEDL")
        TWTRTwitter.sharedInstance().logIn(with: self) { (session, error) in
            print(error.debugDescription)
            if (session != nil) {
                
                print(session?.userName as Any)
                print(session?.userID as Any)
                print(session?.authToken)
                print(session)
                
            } else {
                print("error")
                
            }
        }
        
       /* TWTRTwitter.sharedInstance().logIn { (session, error)-> Void in
            
            print(error!)
            if (session != nil) {
                
                print(session?.userName as Any)
                print(session?.userID as Any)
            } else {
                print("error")
                
            }
        }*/
    }
    
    //Google Login
    
    func googleSignIn(){
        GIDSignIn.sharedInstance().signIn()
        
    }
    @IBAction func gmail_touch(_ sender: Any) {
        
        if !Reachability.isConnectedToNetwork() {
            let banner = NotificationBanner(title: "Alert", subtitle: Defines.noInterNet , style: .danger)
            banner.show(queuePosition: .front)
            return
        }
        
        self.googleSignIn()
        
    }
    
    
    func socialAction(data:[String:String]) {
        
        if !Reachability.isConnectedToNetwork() {
            let banner = NotificationBanner(title: "Alert", subtitle: Defines.noInterNet , style: .danger)
            banner.show(queuePosition: .front)
            return
        }
        
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        print(data)
        DataProvider.sharedInstance.getDataUsingPost(path: Defines.ServerUrl + Defines.socialLogin, dataDict: data, { (json) in
            print(json)
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if json["status"].stringValue == "true" {
                let banner = NotificationBanner(title: "Confirmation", subtitle: "Login successful." , style: .success)
                banner.show(queuePosition: .front)
                
                UserData._id = json["data"]["_id"].stringValue
                UserData.city = json["data"]["city"].stringValue
                UserData.country = json["data"]["country"].stringValue
                UserData.dob = json["data"]["dob"].stringValue
                UserData.email = json["data"]["email"].stringValue
                UserData.gender = json["data"]["gender"].stringValue
                UserData.mobile = json["data"]["mobile"].stringValue
                UserData.name = json["data"]["first_name"].stringValue
                UserData.userName = json["data"]["username"].stringValue
                UserData.userToken = json["token"].stringValue
                
                UserDefaults.standard.setValue(UserData.name, forKey: "firstName")
                UserDefaults.standard.setValue(UserData.email, forKey: "email")
                UserDefaults.standard.setValue(UserData._id, forKey: "Id")
                UserDefaults.standard.setValue(UserData.city, forKey: "city")
                UserDefaults.standard.setValue(UserData.country, forKey: "country")
                UserDefaults.standard.setValue(UserData.dob, forKey: "dob")
                UserDefaults.standard.setValue(UserData.userName, forKey: "userName")
                UserDefaults.standard.setValue(UserData.gender, forKey: "gender")
                UserDefaults.standard.setValue(UserData.mobile, forKey: "mobile")
                UserDefaults.standard.setValue(UserData.userToken, forKey: "token")
                UserDefaults.standard.setValue("yes", forKey: "login")
                
                
                self.performSegue(withIdentifier: "login", sender: nil)
            }else if json["status"].stringValue == "false" {
                let banner = NotificationBanner(title: "Alert", subtitle: json["msg"].stringValue , style: .danger)
                banner.show(queuePosition: .front)
            }
        }) { (error) in
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            print(error)
        }
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        /*if textField == usernameText {
            if !Utility.isValidEmail(email: textField.text!) {
                let banner = NotificationBanner(title: "Alert", subtitle: "Please fill vaild email address." , style: .danger)
                banner.show(queuePosition: .front)
            }
        }*/
    }
    
}
