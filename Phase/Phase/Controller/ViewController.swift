//
//  ViewController.swift
//  Phase
//
//  Created by Mahabir on 15/02/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        
        self.checkAlreadyLogin()
    }

    func checkAlreadyLogin() {
        
        var Login:String = ""
        if let temp  = UserDefaults.standard.string(forKey: "login") {
            Login = temp
        }
        // check user alerdy login or not
        
        if Login == "yes" {
            if let temp = UserDefaults.standard.string(forKey: "firstName") {
                UserData.name = temp
            }
            if let temp = UserDefaults.standard.string(forKey: "city") {
                UserData.city = temp
            }
            if let temp =  UserDefaults.standard.string(forKey: "email") {
                UserData.email = temp
            }
            if let temp = UserDefaults.standard.string(forKey: "Id") {
                UserData._id = temp
            }
            if let temp = UserDefaults.standard.string(forKey: "country") {
                UserData.country = temp
            }
            if let temp = (UserDefaults.standard.string(forKey: "dob"))  {
                UserData.dob = temp
            }
            if let temp = UserDefaults.standard.string(forKey: "gender") {
                UserData.gender = temp
            }
            if let temp = UserDefaults.standard.string(forKey: "mobile") {
                UserData.mobile = temp
            }
            
            if let temp = UserDefaults.standard.string(forKey: "userName") {
                UserData.userName = temp
            }
            if let temp = UserDefaults.standard.string(forKey: "token") {
                UserData.userToken = temp
            }
            
            self.performSegue(withIdentifier: "autoLogin", sender: nil)
            
        }
    }
}

