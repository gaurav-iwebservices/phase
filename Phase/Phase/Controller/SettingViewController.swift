//
//  SettingViewController.swift
//  Phase
//
//  Created by Mahabir on 15/02/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit
import GoogleSignIn

class SettingViewController: UIViewController {

    var backView:UIView!
    @IBOutlet weak var tableView: UITableView!
    var addController = ChatViewController()
    var settingMenu = ["App Settings", "Notification Settings", "Accounts Settings", "Help & Supports", "Terms & Policies", "Report a Problem", "About","Logout"]
    var settingMenuIcon = ["setting-1","notification_settings","account_setting","help-support","terms&polices","","",""]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorStyle = .singleLine
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
       self.title = "SETTINGS"
    }
    
    
    @IBAction func chatAction(_ sender: UIBarButtonItem) {
        
        backView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        backView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        //self.view.addSubview(view)
        UIApplication.shared.keyWindow?.addSubview(backView)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        addController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        addController.delegate = self
        
        addController.view.frame = CGRect(x: UIScreen.main.bounds.width/4 , y: 0, width: UIScreen.main.bounds.width - UIScreen.main.bounds.width/4 , height: UIScreen.main.bounds.height)
        
        let transition = CATransition()
        let withDuration = 1.0
        transition.duration = withDuration
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        addController.view.layer.add(transition, forKey: kCATransition)
        backView.addSubview(addController.view)
        let TapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hideChatView(sender:)))
        self.backView.addGestureRecognizer(TapRecognizer)
    }
    
    @objc func hideChatView(sender:UIGestureRecognizer) {
        self.closeWindow()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
//MARK:- Tableview Extension

extension SettingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell", for: indexPath) as! SettingCell
        cell.menuNameLabel.text = settingMenu[indexPath.row]
        cell.menuImageView.image = UIImage(named: settingMenuIcon[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 7 {
            let title = "Are you sure you want to logout?"
            
            let refreshAlert = UIAlertController(title: "Alert", message: title, preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                self.logoutAction()
            }))
            refreshAlert.addAction(UIAlertAction(title: "No", style: .default, handler: { (action: UIAlertAction!) in
                
            }))
            
            present(refreshAlert, animated: true, completion: nil)
        }
    }
    
  
    
    func logoutAction() {
        
        if let temp = UserDefaults.standard.string(forKey: "google") {
            if temp == "yes" {
                GIDSignIn.sharedInstance().signOut()
                UserDefaults.standard.setValue("no", forKey: "google")
            }
        }
    
        UserDefaults.standard.setValue("", forKey: "firstName")
        UserDefaults.standard.setValue("", forKey: "email")
        UserDefaults.standard.setValue("", forKey: "Id")
        UserDefaults.standard.setValue("", forKey: "city")
        UserDefaults.standard.setValue("", forKey: "country")
        UserDefaults.standard.setValue("", forKey: "dob")
        UserDefaults.standard.setValue("", forKey: "userName")
        UserDefaults.standard.setValue("", forKey: "gender")
        UserDefaults.standard.setValue("", forKey: "mobile")
        UserDefaults.standard.setValue("", forKey: "token")
        UserDefaults.standard.setValue("no", forKey: "login")
        
        let testController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as! ViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let nav = UINavigationController(rootViewController: testController)
        appDelegate.window?.rootViewController = nav
    }
    
}



extension SettingViewController: chatDelegate {
    
    func closeWindow() {
        addController.view.layer.removeAllAnimations()
        UIView.animate(withDuration: 1.0, animations: {
            self.addController.view.frame.origin.x = 700
            
        }, completion: {(finished : Bool) in
            if(finished)
            {
                self.backView.removeFromSuperview()
                self.addController.willMove(toParentViewController: nil)
                self.addController.view.removeFromSuperview()
                self.addController.removeFromParentViewController()
            }
        })
        
        
    }
}

