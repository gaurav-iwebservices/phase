//
//  NotificationViewController.swift
//  Phase
//
//  Created by Mahabir on 15/02/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//8373953957

import UIKit

class NotificationViewController: UIViewController {

    var addController = ChatViewController()
    var chatView:ChatView!
    var backView:UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "NOTIFICATIONS"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func chatAction(_ sender: UIBarButtonItem) {
        
        backView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        backView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        //self.view.addSubview(view)
        UIApplication.shared.keyWindow?.addSubview(backView)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        addController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        addController.delegate = self
        
        addController.view.frame = CGRect(x: UIScreen.main.bounds.width/4 , y: 0, width: UIScreen.main.bounds.width - UIScreen.main.bounds.width/4 , height: UIScreen.main.bounds.height)
        
        let transition = CATransition()
        let withDuration = 1.0
        transition.duration = withDuration
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        addController.view.layer.add(transition, forKey: kCATransition)
        backView.addSubview(addController.view)
        let TapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hideChatView(sender:)))
        self.backView.addGestureRecognizer(TapRecognizer)
    }
    
    @objc func hideChatView(sender:UIGestureRecognizer) {
        self.closeWindow()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NotificationViewController: chatDelegate {
    
    func closeWindow() {
        addController.view.layer.removeAllAnimations()
        UIView.animate(withDuration: 1.0, animations: {
            self.addController.view.frame.origin.x = 700
            
        }, completion: {(finished : Bool) in
            if(finished)
            {
                self.backView.removeFromSuperview()
                self.addController.willMove(toParentViewController: nil)
                self.addController.view.removeFromSuperview()
                self.addController.removeFromParentViewController()
            }
        })
        
        
    }
}
