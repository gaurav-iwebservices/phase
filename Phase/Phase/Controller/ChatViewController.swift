//
//  ChatViewController.swift
//  Phase
//
//  Created by Mahabir on 28/02/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit

protocol chatDelegate {
    func closeWindow()
}

class ChatViewController: UIViewController {
    @IBOutlet weak var searchBackgroundView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var friendsArray:[String] = ["Jhon doe","Marry","Peter"]
    var sectionArr:[String] = ["AROUND","FRIENDS","GROUPS"]
    
    var delegate:chatDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBackgroundView.layer.cornerRadius = searchBackgroundView.frame.height/2
        searchBackgroundView.layer.masksToBounds = true
        searchBackgroundView.layer.borderColor = UIColor.white.cgColor
        searchBackgroundView.layer.borderWidth = 1
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
    }

   
    @IBAction func swipeAction(_ sender: UISwipeGestureRecognizer) {
        delegate?.closeWindow()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ChatViewController:UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friendsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell", for: indexPath) as! ChatCell
        cell.userNameLabel.text = friendsArray[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatHeaderCell") as! ChatHeaderCell
        cell.headerLabel.text = sectionArr[section]
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 44
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let nextView = storyBoard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
              //  nextView.userId = peopleId[indexPath.row]
                // nextView.dataArr = searchResult[indexPath.section]
                //  nextView.navigationTitle = searchText.text! + sectionTitle[indexPath.section]
                self.navigationController?.pushViewController(nextView, animated: true)
}
    
}
