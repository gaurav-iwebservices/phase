//
//  GalleryViewController.swift
//  Phase
//
//  Created by Mahabir on 04/04/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import AlamofireImage

class GalleryViewController: UIViewController {

    var sectionInsets =  UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    let itemsPerRow:CGFloat = 3
    @IBOutlet weak var collectionView: UICollectionView!
    var imagePath:[String] = []
    var userId:String!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "GALLERY"
        self.getUserGallery()
    }
    
    func getUserGallery() {
        
        
        
        if  !Reachability.isConnectedToNetwork() {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.noInterNet)
            return
        }
        
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        DataProvider.sharedInstance.getDataUsingGet(path: Defines.ServerUrl + Defines.gallery + userId, { (json) in
            print(json)
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            if json["status"].stringValue == "true" {
                self.imagePath = []
                for val in json["data"].arrayValue {
                    self.imagePath.append(val["gallery_picture"].stringValue)
                }
                self.collectionView.reloadData()
            }
        }) { (error) in
            print(error)
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension GalleryViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagePath.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryImageCell", for: indexPath) as! GalleryImageCell
        print("http://67.205.173.26:3000/galleryImg/" + imagePath[indexPath.row])
        let url = URL(string: "http://67.205.173.26:3000/postImg/" + imagePath[indexPath.row])
        let placeholder = UIImage(named: "default")
        let filter = AspectScaledToFillSizeFilter(size: cell.userPic.frame.size)
        cell.userPic.af_setImage(withURL: url! , placeholderImage: placeholder, filter: filter)

        
        return cell
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
            let paddingSpace = (5 ) * (itemsPerRow + 1)
            let availableWidth = view.frame.width - paddingSpace
            let widthPerItem = availableWidth / itemsPerRow
        
        //let widthPerItem =  (view.frame.width - 30)/itemsPerRow
        
        let heightPerItem = (UIScreen.main.bounds.height - (self.navigationController?.navigationBar.frame.height)! - (self.tabBarController?.tabBar.frame.height)!)/4
            
            return CGSize(width: widthPerItem , height: heightPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
}
