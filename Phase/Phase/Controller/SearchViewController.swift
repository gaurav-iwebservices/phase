//
//  SearchViewController.swift
//  Phase
//
//  Created by Mahabir on 05/03/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NotificationBannerSwift
import NVActivityIndicatorView

class SearchViewController: UIViewController {
    @IBOutlet weak var searchText: UITextField!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var filterViewConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var postBtn: UIButton!
    @IBOutlet weak var allBtn: UIButton!
    @IBOutlet weak var eventBtn: UIButton!
    @IBOutlet weak var peopleBtn: UIButton!
    var request:DataRequest!
    
    var isAll:Bool = true
    var isPost:Bool = true
    var isEvent:Bool = true
    var isPeople:Bool = true
    
    var event:[String] = []
    var people:[String] = []
    var post:[String] = []
    var friend:[String] = []
    var peopleId:[String] = []
    var searchResult:[[String]] = []
    var sectionTitle:[String] = [" in peoples"," in posts"," in events"]
    var searchDic:[String:[String]] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true
        backView.layer.borderWidth = 1
        backView.layer.borderColor = UIColor.white.cgColor
        searchText.tintColor = .white
        searchText.delegate = self
        
        filterView.layer.cornerRadius = 5
        filterView.layer.masksToBounds = true
        filterView.dropShawdow()
        filterView.isHidden = true
        
        tableView.separatorStyle = .singleLine
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        
        allBtn.addTarget(self, action: #selector(self.allAction(sender:)), for: UIControlEvents.touchUpInside)
        postBtn.addTarget(self, action: #selector(self.postAction(sender:)), for: UIControlEvents.touchUpInside)
        eventBtn.addTarget(self, action: #selector(self.eventAction(sender:)), for: UIControlEvents.touchUpInside)
        peopleBtn.addTarget(self, action: #selector(self.peopleAction(sender:)), for: UIControlEvents.touchUpInside)
      //  let doubleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hideFilterView(sender:)))
        //self.view.addGestureRecognizer(doubleTapRecognizer)
        
        searchDic = [
            "people":[],
            "post":[],
            "event":[]
        ]
    }
   
    @IBAction func hideFilterView(_ sender: UIButton) {
        filterView.isHidden = true
    }
    
    @IBAction func filterAction(_ sender: UIButton) {
       filterView.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
         self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @objc func allAction(sender:UIButton) {
        if isAll == true {
            isAll = false
            sender.setImage(#imageLiteral(resourceName: "unchecked"), for: UIControlState.normal)
        }else{
            isAll = true
            sender.setImage(#imageLiteral(resourceName: "checked"), for: UIControlState.normal)
        }
    }
    
    @objc func eventAction(sender:UIButton) {
        if isEvent == true {
            isEvent = false
            sender.setImage(#imageLiteral(resourceName: "unchecked"), for: UIControlState.normal)
            searchDic.removeValue(forKey: "event")
            print(searchDic)
            self.filterData(dataDict: searchDic)
        }else{
            isEvent = true
            sender.setImage(#imageLiteral(resourceName: "checked"), for: UIControlState.normal)
            searchDic["event"] = self.event
            print(searchDic)
            self.filterData(dataDict: searchDic)
        }
    }
    
    @objc func postAction(sender:UIButton) {
        if isPost == true {
            isPost = false
            sender.setImage(#imageLiteral(resourceName: "unchecked"), for: UIControlState.normal)
            searchDic.removeValue(forKey: "post")
            print(searchDic)
            self.filterData(dataDict: searchDic)
        }else{
            isPost = true
            sender.setImage(#imageLiteral(resourceName: "checked"), for: UIControlState.normal)
            searchDic["post"] = self.post
            print(searchDic)
            self.filterData(dataDict: searchDic)
        }
    }
    
    @objc func peopleAction(sender:UIButton) {
        if isPeople == true {
            isPeople = false
            sender.setImage(#imageLiteral(resourceName: "unchecked"), for: UIControlState.normal)
            searchDic.removeValue(forKey: "people")
            print(searchDic)
            self.filterData(dataDict: searchDic)
        }else{
            isPeople = true
            sender.setImage(#imageLiteral(resourceName: "checked"), for: UIControlState.normal)
            searchDic["people"] = self.people
            print(searchDic)
            self.filterData(dataDict: searchDic)
        }
    }
    
    
    func filterData(dataDict:Dictionary<String, [String]>) {
        searchResult = []
        sectionTitle = []
        if dataDict["people"] != nil {
            searchResult.append(dataDict["people"]!)
            sectionTitle.append(" in peoples")
        }
        
        if dataDict["post"] != nil {
            searchResult.append(dataDict["post"]!)
            sectionTitle.append(" in posts")
        }
        
        if dataDict["event"] != nil {
            searchResult.append(dataDict["event"]!)
            sectionTitle.append(" in event")
        }
        
        self.tableView.reloadData()
    }
    
    
    func getSearchData(name:String) {
       
        let dataDict:[String:String] = ["name":name,"user_id":UserData._id]
        
        if request != nil {
            request.cancel()
        }
        
        if  !Reachability.isConnectedToNetwork() {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.noInterNet)
            return
            
        }
       // let activityData = ActivityData()
       // NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)

        
       request =  Alamofire.request(Defines.ServerUrl + Defines.search, method: .post, parameters: dataDict, encoding: JSONEncoding.default).responseJSON { response in
         //   NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            switch response.result {
            case .success:
                if let value = response.result.value {
                    self.searchResult = []
                    self.people = []
                    self.post = []
                    self.event = []
                    self.friend = []
                    let json = JSON(value)
                    print(json)
                    if json["status"].stringValue == "true" {
                        for i in 0..<json["people"].arrayValue.count {
                            self.people.append(json["people"][i]["first_name"].stringValue + " " + json["people"][i]["last_name"].stringValue)
                            self.peopleId.append(json["people"][i]["_id"].stringValue)
                            //status  0 = pending, 1 = friend
                            if json["people"][i]["friends"][0]["status"].stringValue != "" {
                                self.friend.append(json["people"][i]["friends"][0]["status"].stringValue)
                            }else{
                                self.friend.append("-1")
                            }
                        }
                        
                        for i in 0..<json["post"].arrayValue.count {
                            self.post.append(json["post"][i]["title"].stringValue )
                        }
                        
                        for i in 0..<json["event"].arrayValue.count {
                            self.event.append(json["event"][i]["title"].stringValue )
                        }
                    }
                    
                    if self.searchDic["people"] != nil {
                        self.searchDic["people"] = self.people
                    }
                    
                    if self.searchDic["post"] != nil {
                        self.searchDic["post"] = self.post
                    }
                    
                    if self.searchDic["event"] != nil {
                        self.searchDic["event"] = self.event
                    }
                    
                    print(self.searchDic)
                   // self.searchResult = [self.people,self.post,self.event]
                    //self.tableView.reloadData()
                    self.filterData(dataDict: self.searchDic)
                    
                }
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    
    @objc func addFriendAction(sender:UIButton) {
        
        if sender.titleLabel?.text != "add friend" {
            return
        }
        
        let parameter = [
            "send_from_user_id":UserData._id,
            "send_to_user_id":peopleId[sender.tag],
            "type":"1"
        ]
        
        if  !Reachability.isConnectedToNetwork() {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.noInterNet)
            return
            
        }
        print(parameter)
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        DataProvider.sharedInstance.getDataUsingPost(path: Defines.ServerUrl + Defines.sendFriendRequest, dataDict: parameter, { (json) in
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            print(json)
            
            if json["status"].stringValue == "true" {
                let banner = NotificationBanner(title: "Confirmation", subtitle: json["msg"].stringValue , style: .success)
                banner.show(queuePosition: .front)
                sender.setTitle("Request sent", for: UIControlState.normal)
            
            }else if json["status"].stringValue == "false" {
                let banner = NotificationBanner(title: "Alert", subtitle: json["msg"].stringValue , style: .danger)
                banner.show(queuePosition: .front)
            }
            
        }) { (error) in
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            print(error)
        }
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SearchViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return searchResult.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchResult[section].count == 0 {
            return 1
        }else if searchResult[section].count > 4 {
            return 5
        }
        return searchResult[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if searchResult[indexPath.section].count == 0 {
            let cell = UITableViewCell()
            cell.textLabel?.text = "No result found."
            cell.selectionStyle = .none
            return cell
        }else if indexPath.row == 4 {
            let cell = UITableViewCell()
            cell.textLabel?.text = "See more..."
            cell.selectionStyle = .none
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as! SearchCell
        cell.searchDetailLabel.text = searchResult[indexPath.section][indexPath.row]
        
        cell.addFriendBtn.addTarget(self, action: #selector(self.addFriendAction(sender:)), for: UIControlEvents.touchUpInside)
        if indexPath.section == 0 {
            cell.addFriendBtn.isHidden = false
            cell.addFriendBtn.tag = indexPath.row
            if friend[indexPath.row] == "1" {
                cell.addFriendBtn.setTitle("friend", for: UIControlState.normal)
            }else if friend[indexPath.row] == "0" {
                cell.addFriendBtn.setTitle("pending", for: UIControlState.normal)
            }else {
                cell.addFriendBtn.setTitle("add friend", for: UIControlState.normal)
            }
            
        }else {
            cell.addFriendBtn.isHidden = true
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return searchText.text! + sectionTitle[section]
    }
    

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        print(indexPath.row)
        if indexPath.row == 4 {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let nextView = storyBoard.instantiateViewController(withIdentifier: "Search2ViewController") as! Search2ViewController
            if indexPath.section == 0 {
                nextView.searchType = "people"
                nextView.friend = friend
                nextView.peopleId = peopleId
            }else{
                nextView.searchType = "other"
            }
            nextView.dataArr = searchResult[indexPath.section]
            nextView.navigationTitle = searchText.text! + sectionTitle[indexPath.section]
            self.navigationController?.pushViewController(nextView, animated: false)
        }else{
            if indexPath.section == 0 {
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let nextView = storyBoard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
                nextView.userId = peopleId[indexPath.row]
                // nextView.dataArr = searchResult[indexPath.section]
                //  nextView.navigationTitle = searchText.text! + sectionTitle[indexPath.section]
                self.navigationController?.pushViewController(nextView, animated: true)
            }
        }
    }
}


extension SearchViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.getSearchData(name: textField.text! + string)
        return true
    }
}
