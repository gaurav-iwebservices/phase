//
//  EditDetailsViewController.swift
//  Phase
//
//  Created by Mahabir on 05/04/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit
import NotificationBannerSwift
import NVActivityIndicatorView

class EditDetailsViewController: UIViewController {
    @IBOutlet weak var locationText: UITextField!
    @IBOutlet weak var workText: UITextField!
    @IBOutlet weak var educationText: UITextField!
    
    var location:String!
    var work:String!
    var education:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        locationText.text = location
        workText.text = work
        educationText.text = education
        
        locationText.delegate = self
        workText.delegate = self
        educationText.delegate = self
        
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        self.saveProfile()
    }
    
    func saveProfile() {
        let parameter:[String:String] = [
            "user_id":UserData._id,
            "profession":workText.text!,
            "education":educationText.text!,
            "location":locationText.text!
        ]
        
        if  !Reachability.isConnectedToNetwork() {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.noInterNet)
            return
        }
        
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        DataProvider.sharedInstance.getDataUsingPost(path: Defines.ServerUrl + Defines.editProfile, dataDict: parameter, { (json) in
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            print(json)
            
            if json["status"].stringValue == "true" {
                let banner = NotificationBanner(title: "Confirmation", subtitle: json["msg"].stringValue , style: .success)
                banner.show(queuePosition: .front)
                
            }else if json["status"].stringValue == "false" {
                let banner = NotificationBanner(title: "Alert", subtitle: json["msg"].stringValue , style: .danger)
                banner.show(queuePosition: .front)
            }
        }) { (error) in
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            print(error)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EditDetailsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
