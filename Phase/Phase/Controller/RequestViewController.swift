//
//  RequestViewController.swift
//  Phase
//
//  Created by Mahabir on 15/02/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import NotificationBannerSwift

class RequestViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var addController = ChatViewController()
    var requestArr:[Request] = []
    var backView:UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "REQUESTS"
        tableView.separatorStyle = .none
        self.tableView.estimatedRowHeight = 170
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.getFriendRequest()
    }

    
    func getFriendRequest() {
        
        if  !Reachability.isConnectedToNetwork() {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.noInterNet)
            return
            
        }
        
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        DataProvider.sharedInstance.getDataUsingGet(path: Defines.ServerUrl + Defines.friendRequest + UserData._id, { (json) in
             NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            print(json)
            if json["status"].stringValue == "true" {
                self.requestArr = []
                for data in json["data"].arrayValue {
                    self.requestArr.append(Request.init(
                        type: data["type"].stringValue,
                        name: data["send_from_user_id"]["first_name"].stringValue + " " + data["send_from_user_id"]["last_name"].stringValue,
                        friendId: data["send_from_user_id"]["_id"].stringValue,
                        requestId: data["_id"].stringValue,
                        status: data["status"].stringValue
                    ))
                }
            }
            
            self.tableView.reloadData()
            
        }) { (error) in
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            print(error)
        }
    }
   
    @IBAction func chatAction(_ sender: UIBarButtonItem) {
        
        backView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        backView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        //self.view.addSubview(view)
        UIApplication.shared.keyWindow?.addSubview(backView)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        addController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        addController.delegate = self
        
        addController.view.frame = CGRect(x: UIScreen.main.bounds.width/4 , y: 0, width: UIScreen.main.bounds.width - UIScreen.main.bounds.width/4 , height: UIScreen.main.bounds.height)
        
        let transition = CATransition()
        let withDuration = 1.0
        transition.duration = withDuration
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        addController.view.layer.add(transition, forKey: kCATransition)
        backView.addSubview(addController.view)
        let TapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hideChatView(sender:)))
        self.backView.addGestureRecognizer(TapRecognizer)
    }
    
    @objc func hideChatView(sender:UIGestureRecognizer) {
        self.closeWindow()
    }
    
    @objc func acceptRequestAction(sender:UIButton) {
        let parameter:[String:String] = [
            "id": requestArr[sender.tag].requestId,
            "status":"1"
        ]
        
        if  !Reachability.isConnectedToNetwork() {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.noInterNet)
            return
            
        }
        
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        DataProvider.sharedInstance.getDataUsingPost(path: Defines.ServerUrl + Defines.changeFriendStatus, dataDict: parameter, { (json) in
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            if json["status"].stringValue == "true" {
                self.getFriendRequest()
                let banner = NotificationBanner(title: "Confirmation", subtitle: json["msg"].stringValue , style: .success)
                banner.show(queuePosition: .front)
            }else if json["status"].stringValue == "false" {
                let banner = NotificationBanner(title: "Alert", subtitle: json["msg"].stringValue , style: .danger)
                banner.show(queuePosition: .front)
            }
            print(json)
        }) { (error) in
           NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            print(error)
        }
    }
    
    @objc func rejectRequestAction(sender:UIButton) {
        let parameter:[String:String] = [
            "id": requestArr[sender.tag].requestId,
            "status":"2"
        ]
        
        if  !Reachability.isConnectedToNetwork() {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.noInterNet)
            return
            
        }
        
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        DataProvider.sharedInstance.getDataUsingPost(path: Defines.ServerUrl + Defines.changeFriendStatus, dataDict: parameter, { (json) in
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            if json["status"].stringValue == "true" {
                let banner = NotificationBanner(title: "Confirmation", subtitle: json["msg"].stringValue , style: .success)
                banner.show(queuePosition: .front)
            }else if json["status"].stringValue == "false" {
                let banner = NotificationBanner(title: "Alert", subtitle: json["msg"].stringValue , style: .danger)
                banner.show(queuePosition: .front)
            }
            print(json)
        }) { (error) in
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            print(error)
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
//MARK:- Table view extension

extension RequestViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return requestArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestCell", for: indexPath) as! RequestCell
        //cell.setGradient(view: cell.backgroundContentView)
        /*if indexPath.row % 2 == 0 {
            cell.messageLabel.text = "Richard has invited you to join soccerclub"
            let gradient:CAGradientLayer = CAGradientLayer()
            gradient.colors = [UIColor(red: 255.0/255.0, green: 160.0/255.0, blue: 163.0/255.0, alpha: 1.0).cgColor , UIColor(red: 255.0/255.0, green: 222.0/255.0, blue: 218.0/255.0, alpha: 1.0).cgColor]
            gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
            gradient.frame = cell.backgroundContentView.bounds
            cell.backgroundContentView.layer.insertSublayer(gradient, at: 0)
            
        }else if indexPath.row % 3 == 0 {
            cell.messageLabel.text = "Mark has invited you to join group."
            let gradient:CAGradientLayer = CAGradientLayer()
            gradient.colors = [UIColor(red: 56.0/255.0, green: 178.0/255.0, blue: 201.0/255.0, alpha: 1.0).cgColor , UIColor(red: 177.0/255.0, green: 239.0/255.0, blue: 252.0/255.0, alpha: 1.0).cgColor]
            gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
            gradient.frame = cell.backgroundContentView.bounds
            cell.backgroundContentView.layer.insertSublayer(gradient, at: 0)
            
        }else{*/
            cell.messageLabel.text = "\(requestArr[indexPath.row].name!) wants to join your network."
            let gradient:CAGradientLayer = CAGradientLayer()
            gradient.colors = [UIColor(red: 112.0/255.0, green: 147.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor , UIColor(red: 213.0/255.0, green: 230.0/255.0, blue: 255.0/255.0, alpha: 1.0).cgColor]
            gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
            gradient.frame = cell.backgroundContentView.bounds
            cell.backgroundContentView.layer.insertSublayer(gradient, at: 0)
            cell.acceptBtn.tag = indexPath.row
            cell.rejectBtn.tag = indexPath.row
            cell.acceptBtn.addTarget(self, action: #selector(self.acceptRequestAction(sender:)), for: UIControlEvents.touchUpInside)
            cell.rejectBtn.addTarget(self, action: #selector(self.rejectRequestAction(sender:)), for: UIControlEvents.touchUpInside)
       // }
        
        return cell
    }
}

extension RequestViewController: chatDelegate {
    
    func closeWindow() {
        addController.view.layer.removeAllAnimations()
        UIView.animate(withDuration: 1.0, animations: {
            self.addController.view.frame.origin.x = 700
            
        }, completion: {(finished : Bool) in
            if(finished)
            {
                self.backView.removeFromSuperview()
                self.addController.willMove(toParentViewController: nil)
                self.addController.view.removeFromSuperview()
                self.addController.removeFromParentViewController()
            }
        })
        
        
    }
}

