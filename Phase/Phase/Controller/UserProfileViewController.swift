//
//  UserProfileViewController.swift
//  Phase
//
//  Created by Mahabir on 27/03/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit
import AlamofireImage
import NVActivityIndicatorView
import NotificationBannerSwift

class UserProfileViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var userId:String!
    var name:String = ""
    var userName:String = ""
    var coverPic:String = ""
    var profilePic:String = ""
    var profession:String = ""
    var dob:String = ""
    var education:String = ""
    var city:String = ""
    var emailAddress:String = ""
    var mobile:String = ""
    var gender:String = ""
    var location:String = ""
    var postArr:[Post] = []
    var postId:String!
    
    var profilePicImageView:UIImageView!
    var CoverPicImageView:UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.separatorStyle = .none
        self.getUserProfileData()
        self.getPost()
    }
    
    func getUserProfileData() {
        
        if  !Reachability.isConnectedToNetwork() {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.noInterNet, withError: nil, onClose: {
                return
            })
        }
        
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        DataProvider.sharedInstance.getDataUsingGet(path: Defines.ServerUrl + Defines.getProfile + userId, { (json) in
            print(json)
            self.userName = json["data"]["username"].stringValue
            self.name = json["data"]["first_name"].stringValue
            self.coverPic = json["data"]["cover_picture"].stringValue
            self.profilePic = json["data"]["profile_pic"].stringValue
            self.profession = json["data"]["profession"].stringValue
            self.dob = json["data"]["dob"].stringValue
            self.education = json["data"]["education"].stringValue
            self.city = json["data"]["city"].stringValue
            self.emailAddress = json["data"]["email"].stringValue
            self.mobile = json["data"]["mobile"].stringValue
            self.gender = json["data"]["gender"].stringValue
            self.location = json["data"]["location"].stringValue
             NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            self.tableView.reloadData()
            
        }) { (error) in
            print(error)
             NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        }
    }
    
    func getPost(){
        if  !Reachability.isConnectedToNetwork() {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.noInterNet, withError: nil, onClose: {
                return
            })
        }
        
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        DataProvider.sharedInstance.getDataUsingGet(path: Defines.ServerUrl + Defines.getPost + userId, { (json) in
            print(json)
            self.postArr = []
            if json["status"].stringValue == "true" {
                for i in 0..<json["data"].arrayValue.count {
                    self.postArr.append(Post.init(
                        postTitle: json["data"][i]["title"].stringValue,
                        lat: json["data"][i]["latitute"].stringValue,
                        long: json["data"][i]["logitute"].stringValue,
                        _id: json["data"][i]["_id"].stringValue,
                        createdDate: json["data"][i]["created_date"].stringValue,
                        type: json["data"][i]["type"].stringValue,
                        fileType: json["data"][i]["file_type"].stringValue,
                        userName: json["data"][i]["user_id"]["first_name"].stringValue + " " + json["data"][i]["user_id"]["middle_name"].stringValue + " " + json["data"][i]["user_id"]["last_name"].stringValue,
                        userId: json["data"][i]["user_id"][""].stringValue,
                        picturePath: json["path"].stringValue + "/" + json["data"][i]["picture"].stringValue
                    ))
                }
                 NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                self.tableView.reloadData()
                print(self.postArr)
            }
        }) { (error) in
            print(error)
             NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        }
    }
    
    
    @objc func likePostAction(sender:UIButton) {
        let origImage = UIImage(named: "like")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        sender.setImage(tintedImage, for: .normal)
        var status:String = "1"
        if sender.tintColor == .red {
            sender.tintColor = .black
            status = "0"
        }else{
            sender.tintColor = .red
            status = "1"
        }
        
        let parameter:[String:String] = [
            "user_id":UserData._id,
            "post_id":postArr[sender.tag]._id,
            "status":status
        ]
        
        if  !Reachability.isConnectedToNetwork() {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.noInterNet, withError: nil, onClose: {
                return
            })
        }
        
        DataProvider.sharedInstance.getDataUsingPost(path: Defines.ServerUrl + Defines.like, dataDict: parameter, { (json) in
            print(json)
            if json["status"].stringValue == "true" {
                let banner = NotificationBanner(title: "Confirmation", subtitle: json["msg"].stringValue  , style: .success)
                banner.show(queuePosition: .front)
            }else if json["status"].stringValue == "false" {
                let banner = NotificationBanner(title: "Alert", subtitle: json["msg"].stringValue , style: .danger)
                banner.show(queuePosition: .front)
            }
        }) { (error) in
            print(error)
        }
    }
    
    @objc func galleryAction(sender:UITextField) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let nextView = storyboard.instantiateViewController(withIdentifier: "GalleryViewController") as! GalleryViewController
        nextView.userId = userId
        self.navigationController?.pushViewController(nextView, animated: true)
        
    }

    

    func resizeImage(image:UIImage, targetSize:CGSize) -> UIImage? {
        let originalSize = image.size
        
        let widthRatio = targetSize.width / originalSize.width
        let heightRatio = targetSize.height / originalSize.height
        
        let ratio = min(widthRatio, heightRatio)
        
        let newSize = CGSize(width: originalSize.width * ratio, height: originalSize.height * ratio)
        
        // preparing rect for new image size
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, UIScreen.main.scale)
        image.draw(in: rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UserProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postArr.count + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfilePictureCell", for: indexPath) as! ProfilePictureCell
            cell.nameLabel.text = self.name
            self.profilePicImageView = cell.profiePicImage
            self.CoverPicImageView = cell.coverImage
            
            if coverPic != "" {
                let url = URL(string: "http://67.205.173.26:3000/profileImg/" + coverPic)
                let placeholder = UIImage(named: "default")
                let filter = AspectScaledToFillSizeFilter(size: cell.coverImage.frame.size)
                cell.coverImage.af_setImage(withURL: url! , placeholderImage: placeholder, filter: filter)
            }
            
            if profilePic != "" {
                let url = URL(string: "http://67.205.173.26:3000/profileImg/" + profilePic)
                let placeholder = UIImage(named: "default")
                let filter = AspectScaledToFillSizeFilter(size: cell.profiePicImage.frame.size)
                cell.profiePicImage.af_setImage(withURL: url! , placeholderImage: placeholder, filter: filter)
            }
            
            cell.selectionStyle = .none
            return cell
        }/*else if indexPath.row == 1 {
             let cell = tableView.dequeueReusableCell(withIdentifier: "AddFriendCell", for: indexPath) as! AddFriendCell
             cell.selectionStyle = .none
             return cell
             }*/
        else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FriendInfoCell", for: indexPath) as! FriendInfoCell
            cell.educationLabel.text = self.education
            cell.locationLabel.text = self.location
            cell.professionLabel.text = self.profession
            cell.photos.addTarget(self, action: #selector(self.galleryAction(sender:)), for: UIControlEvents.touchUpInside)
            
            cell.selectionStyle = .none
            return cell
        }/*else if indexPath.row == 2 {
             let cell = tableView.dequeueReusableCell(withIdentifier: "FriendInfoCell", for: indexPath) as! FriendInfoCell
             cell.selectionStyle = .none
             return cell
         }*/else{
            if postArr[indexPath.row - 2].fileType == "0" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "StatusCell", for: indexPath) as! StatusCell
                cell.statusLabel.text = postArr[indexPath.row - 2].postTitle
                cell.userNameLabel.text = postArr[indexPath.row - 2].userName
                cell.likeButton.addTarget(self, action: #selector(self.likePostAction(sender:)), for: UIControlEvents.touchUpInside)
                cell.likeButton.tag = indexPath.row - 2
                cell.dropdownBtn.tag = indexPath.row - 2
                cell.dropdownBtn.isHidden = true
                cell.selectionStyle = .none
                return cell
            }else if postArr[indexPath.row - 2].fileType == "1" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "StatusCell2", for: indexPath) as! StatusCell2
                cell.dropDownBtn.tag = indexPath.row - 2
                cell.dropDownBtn.isHidden = true
                cell.likeBtn.addTarget(self, action: #selector(self.likePostAction(sender:)), for: UIControlEvents.touchUpInside)
                cell.likeBtn.tag = indexPath.row - 2
                // cell.updateImageView.image = resizeImage(image: #imageLiteral(resourceName: "tom2"), targetSize: cell.updateImageView.frame.size)
                print(Defines.ServerUrl + postArr[indexPath.row - 2].picturePath)
                // cell.updateImageView.downloadedFrom(url: URL(string: "http://67.205.173.26:3000/" + postArr[indexPath.row].picturePath)!)
                let url = URL(string: "http://67.205.173.26:3000/" + postArr[indexPath.row - 2].picturePath)
                let placeholder = UIImage(named: "default")
                
                let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                    size: cell.updateImageView.frame.size,
                    radius: 20.0
                )
                
                // let filter = AspectScaledToFillSizeFilter(size: cell.updateImageView.frame.size)
                cell.updateImageView.af_setImage(withURL: url! , placeholderImage: placeholder, filter: filter)
                
                cell.statusLabel.text = postArr[indexPath.row - 2].postTitle
                cell.userNameLabel.text = postArr[indexPath.row - 2].userName
                //cell.updateImageView.image = UIImage(named: "tom2")
                cell.selectionStyle = .none
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "StatusCell3", for: indexPath) as! StatusCell3
                cell.video(url: URL(string: "http://67.205.173.26:3000/" + postArr[indexPath.row - 2].picturePath)!)
                cell.dropDownBtn.tag = indexPath.row - 2
                cell.dropDownBtn.isHidden = true
                cell.likeBtn.addTarget(self, action: #selector(self.likePostAction(sender:)), for: UIControlEvents.touchUpInside)
                cell.likeBtn.tag = indexPath.row - 2
                cell.statusLabel.text = postArr[indexPath.row - 2].postTitle
                cell.userNameLabel.text = postArr[indexPath.row - 2].userName
                cell.selectionStyle = .none
                return cell
            }
        }
    }
}
