//
//  FriendsViewController.swift
//  Phase
//
//  Created by Mahabir on 16/02/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import AlamofireImage
import NotificationBannerSwift

class FriendsViewController: UIViewController, IndicatorInfoProvider {

    @IBOutlet weak var tableView: UITableView!
    var postArr:[Post] = []
    var postId:String!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.separatorStyle = .none
        self.getPost()
    }

    public func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "FRIENDS")
    }
    
    @objc func dropDownAction(sender:UIButton) {
        self.postId = self.postArr[sender.tag]._id
        self.dropDown(sender, selector: ["Edit","Delete"])
    }
    
    func deletePost() {
        
        DataProvider.sharedInstance.getDataUsingGet(path: Defines.ServerUrl + Defines.deletePost + self.postId, { (json) in
            print(json)
            if json["status"].stringValue == "true" {
                let banner = NotificationBanner(title: "Confirmation", subtitle: json["msg"].stringValue  , style: .success)
                banner.show(queuePosition: .front)
                self.deletePost()
            }else if json["status"].stringValue == "false" {
                let banner = NotificationBanner(title: "Alert", subtitle: json["msg"].stringValue , style: .danger)
                banner.show(queuePosition: .front)
            }
            
        }) { (error) in
            print(error)
        }
    }
    
    func getPost(){
        
        if  !Reachability.isConnectedToNetwork() {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.noInterNet, withError: nil, onClose: {
                return
            })
        }
        
        DataProvider.sharedInstance.getDataUsingGet(path: Defines.ServerUrl + Defines.getPost + UserData._id, { (json) in
            print(json)
            self.postArr = []
            if json["status"].stringValue == "true" {
                for i in 0..<json["data"].arrayValue.count {
                    self.postArr.append(Post.init(
                        postTitle: json["data"][i]["title"].stringValue,
                        lat: json["data"][i]["latitute"].stringValue,
                        long: json["data"][i]["logitute"].stringValue,
                        _id: json["data"][i]["_id"].stringValue,
                        createdDate: json["data"][i]["created_date"].stringValue,
                        type: json["data"][i]["type"].stringValue,
                        fileType: json["data"][i]["file_type"].stringValue,
                        userName: json["data"][i]["user_id"]["first_name"].stringValue + " " + json["data"][i]["user_id"]["middle_name"].stringValue + " " + json["data"][i]["user_id"]["last_name"].stringValue,
                        userId: json["data"][i]["user_id"][""].stringValue,
                        picturePath: json["path"].stringValue + "/" + json["data"][i]["picture"].stringValue
                    ))
                }
                self.tableView.reloadData()
                print(self.postArr)
            }
        }) { (error) in
            print(error)
        }
    }
    

    @objc func likePostAction(sender:UIButton) {
        let origImage = UIImage(named: "like")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        sender.setImage(tintedImage, for: .normal)
        var status:String = "1"
        if sender.tintColor == .red {
            sender.tintColor = .black
            status = "0"
        }else{
            sender.tintColor = .red
            status = "1"
        }
        
        let parameter:[String:String] = [
            "user_id":UserData._id,
            "post_id":postArr[sender.tag]._id,
            "status":status
        ]
        
        if  !Reachability.isConnectedToNetwork() {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.noInterNet, withError: nil, onClose: {
                return
            })
        }
        
        DataProvider.sharedInstance.getDataUsingPost(path: Defines.ServerUrl + Defines.like, dataDict: parameter, { (json) in
            print(json)
            if json["status"].stringValue == "true" {
                let banner = NotificationBanner(title: "Confirmation", subtitle: json["msg"].stringValue  , style: .success)
                banner.show(queuePosition: .front)
            }else if json["status"].stringValue == "false" {
                let banner = NotificationBanner(title: "Alert", subtitle: json["msg"].stringValue , style: .danger)
                banner.show(queuePosition: .front)
            }
        }) { (error) in
            print(error)
        }
    }
    
    func resizeImage(image:UIImage, targetSize:CGSize) -> UIImage? {
        let originalSize = image.size
        
        let widthRatio = targetSize.width / originalSize.width
        let heightRatio = targetSize.height / originalSize.height
        let ratio = min(widthRatio, heightRatio)
        let newSize = CGSize(width: originalSize.width * ratio, height: originalSize.height * ratio)
        // preparing rect for new image size
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        UIGraphicsBeginImageContextWithOptions(newSize, false, UIScreen.main.scale)
        image.draw(in: rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK:- UITableview delegate

extension FriendsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if postArr[indexPath.row].fileType == "0" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "StatusCell", for: indexPath) as! StatusCell
            cell.statusLabel.text = postArr[indexPath.row].postTitle
            cell.userNameLabel.text = postArr[indexPath.row].userName
            cell.likeButton.addTarget(self, action: #selector(self.likePostAction(sender:)), for: UIControlEvents.touchUpInside)
            cell.likeButton.tag = indexPath.row
            cell.dropdownBtn.tag = indexPath.row
            cell.dropdownBtn.addTarget(self, action: #selector(self.dropDownAction(sender:)), for: UIControlEvents.touchUpInside)
            cell.selectionStyle = .none
            return cell
        }else if postArr[indexPath.row].fileType == "1" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "StatusCell2", for: indexPath) as! StatusCell2
            cell.likeBtn.addTarget(self, action: #selector(self.likePostAction(sender:)), for: UIControlEvents.touchUpInside)
            cell.likeBtn.tag = indexPath.row
            // cell.updateImageView.image = resizeImage(image: #imageLiteral(resourceName: "tom2"), targetSize: cell.updateImageView.frame.size)
            print(Defines.ServerUrl + postArr[indexPath.row].picturePath)
            //cell.updateImageView.downloadedFrom(url: URL(string: "http://67.205.173.26:3000/" + postArr[indexPath.row].picturePath)!)
            let url = URL(string: "http://67.205.173.26:3000/" + postArr[indexPath.row].picturePath)
            let placeholder = UIImage(named: "default")
            
            let filter = AspectScaledToFillSizeFilter(size: cell.updateImageView.frame.size)
            cell.updateImageView.af_setImage(withURL: url! , placeholderImage: placeholder, filter: filter)
            cell.dropDownBtn.tag = indexPath.row
            cell.dropDownBtn.addTarget(self, action: #selector(self.dropDownAction(sender:)), for: UIControlEvents.touchUpInside)
            //cell.updateImageView.af_setI
            
            cell.statusLabel.text = postArr[indexPath.row].postTitle
            cell.userNameLabel.text = postArr[indexPath.row].userName
            //cell.updateImageView.image = UIImage(named: "tom2")
            cell.selectionStyle = .none
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "StatusCell3", for: indexPath) as! StatusCell3
            cell.video(url: URL(string: "http://67.205.173.26:3000/" + postArr[indexPath.row].picturePath)!)
            cell.likeBtn.addTarget(self, action: #selector(self.likePostAction(sender:)), for: UIControlEvents.touchUpInside)
            cell.likeBtn.tag = indexPath.row
            cell.dropDownBtn.tag = indexPath.row
            cell.dropDownBtn.addTarget(self, action: #selector(self.dropDownAction(sender:)), for: UIControlEvents.touchUpInside)
            cell.statusLabel.text = postArr[indexPath.row].postTitle
            cell.userNameLabel.text = postArr[indexPath.row].userName
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let nextView = storyboard.instantiateViewController(withIdentifier: "ShowPostViewController") as! ShowPostViewController
        nextView.postId = postArr[indexPath.row]._id
        nextView.postType = postArr[indexPath.row].fileType
        nextView.statusText = postArr[indexPath.row].postTitle
        nextView.imageUrl = "http://67.205.173.26:3000/" + postArr[indexPath.row].picturePath
        nextView.userName = postArr[indexPath.row].userName
        self.navigationController?.pushViewController(nextView, animated: true)
        
    }
}


extension FriendsViewController : UIPopoverPresentationControllerDelegate,PopViewControllerDelegate {
    
    func dropDown(_ sender:UIButton , selector:[String]) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let popController = storyBoard.instantiateViewController(withIdentifier: "PopViewController") as!  PopViewController
        popController.delegate = self
        popController.arr = selector
        popController.modalPresentationStyle = UIModalPresentationStyle.popover
        popController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
        popController.popoverPresentationController?.delegate = self
        popController.popoverPresentationController?.sourceView = sender
        popController.popoverPresentationController?.sourceRect = sender.bounds
        popController.preferredContentSize = CGSize(width: 100, height: 100)
        self.present(popController, animated: true, completion: nil)
    }
    
    func saveString(_ strText: String) {
        print(strText)
        
        if strText == "Delete" {
            Utility.showMessageDialogWithCancel(onController: self, withTitle: "Alert", withMessage: "Are you sure you want to delete this post?", onClose: {
                self.deletePost()
            })
        }
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle{
        return UIModalPresentationStyle.none
    }
    
}
