//
//  DashboardViewController.swift
//  Phase
//
//  Created by Mahabir on 15/02/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import IQKeyboardManagerSwift
import Alamofire
import NotificationBannerSwift
import SwiftyJSON
import NVActivityIndicatorView
import MobileCoreServices
import AssetsLibrary
import AVFoundation

class DashboardViewController: ButtonBarPagerTabStripViewController {
    var backView:UIView!
    @IBOutlet weak var subViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var postBtn: DesignButton!
    
    @IBOutlet weak var postTypeText: UITextField!
    @IBOutlet weak var textContentViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var statusText: UITextView!
    @IBOutlet weak var placeholderLabel: UILabel!
    @IBOutlet weak var textContentView: UIView!
    @IBOutlet weak var btnStackView: UIStackView!
    @IBOutlet weak var eventBtn: UIButton!
    @IBOutlet weak var videoBtn: UIButton!
    @IBOutlet weak var photoBtn: UIButton!
    var eventView:eventSelectorView!
    var addController = ChatViewController()
    var childOneVC:AroundViewController!
    @IBOutlet weak var uploadImage: UIImageView!
    var imagePicker = UIImagePickerController()
    var chosenImage:UIImage!
    var tmpTextField:UITextField!
    let blueInstagramColor = UIColor.black
    @IBOutlet weak var videoView: UIView!
    var player : AVPlayer?
    var videoUrl:URL!
    var postType:String = "text"
    
    override func viewDidLoad() {
        
        settings.style.buttonBarBackgroundColor = .clear
        settings.style.buttonBarItemBackgroundColor = .clear
        // settings.style.selectedBarBackgroundColor = UIColor(red: 0.0/255.0, green: 182.0/255.0, blue: 223.0/255.0, alpha: 1.0)
        settings.style.selectedBarBackgroundColor = UIColor(red: 127.0/255.0, green: 205.0/255.0, blue: 254.0/255.0, alpha: 1.0)
        
        settings.style.buttonBarItemFont = UIFont(name: "Helvetica", size: 16)!
        
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = UIColor(red: 146/255.0, green: 147/255.0, blue: 156/255.0, alpha: 0.8)
        
        settings.style.buttonBarItemTitleColor =  UIColor(red: 163.0/255.0, green: 163.0/255.0, blue: 163.0/255.0, alpha: 1.0)
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        containerView.isScrollEnabled = false
        
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            // oldCell?.label.textColor = UIColor(red: 146/255.0, green: 147/255.0, blue: 156/255.0, alpha: 0.8)
            oldCell?.label.textColor = UIColor(red: 163.0/255.0, green: 163.0/255.0, blue: 163.0/255.0, alpha: 1.0)
            newCell?.label.textColor = self?.blueInstagramColor
            
        }
        
        super.viewDidLoad()

       self.title = "DASHBOARD"
        self.navigationController?.navigationBar.isHidden = false
        self.textContentView.layer.cornerRadius = self.textContentView.frame.size.height/2
        self.textContentView.layer.borderColor = UIColor(red: 222.0/255.0, green: 222.0/255.0, blue: 222.0/255.0, alpha: 1.0).cgColor
        self.textContentView.layer.borderWidth = 1.0
        self.textContentView.layer.masksToBounds = true
        self.statusText.delegate = self
        self.statusText.tintColor = .black
        self.postTypeText.delegate = self
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        self.photoBtn.addTarget(self, action: #selector(self.changePicture(_:)), for: UIControlEvents.touchUpInside)
        self.videoBtn.addTarget(self, action: #selector(self.takeVideo(_:)), for: UIControlEvents.touchUpInside)
    }
    
    override public func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
       
         childOneVC = storyboard?.instantiateViewController(withIdentifier: "AroundViewController") as! AroundViewController
        let childTwoVC = storyboard?.instantiateViewController(withIdentifier: "FriendsViewController")
        let childThreeVC = storyboard?.instantiateViewController(withIdentifier: "PublicViewController")
        
        return [childOneVC!, childTwoVC! , childThreeVC! ]
    }

    
    func AnimateBackgroundHeight(height:CGFloat, height2:CGFloat, _ successBlock:@escaping ( _ response: Bool )->Void) {
        UIView.animate(withDuration: 1.0, animations: {
            self.subViewHeightConstraint.constant = height // heightCon is the IBOutlet to the constraint
            self.textContentViewConstraint.constant = height2
            self.view.layoutIfNeeded()
            successBlock(true)
            
        })
        
        
        
     /*   UIView.animate(withDuration: 1.0, animations: {
            if height == 70 {
                self.photoBtn.isHidden = true
                self.videoBtn.isHidden = true
                self.eventBtn.isHidden = true
                self.btnStackView.isHidden = true
                self.postBtn.isHidden = true
                self.postTypeText.isHidden = true
            }else if height == 230 {
                self.btnStackView.isHidden = false
                self.photoBtn.isHidden = false
                self.videoBtn.isHidden = false
                self.eventBtn.isHidden = false
                self.postBtn.isHidden = false
                self.postTypeText.isHidden = false
            }
        })*/
        
        
    }
    
    func animation(height:CGFloat, height2:CGFloat){
        UIView.animate(withDuration: 1.0, animations: {
            self.subViewHeightConstraint.constant = height // heightCon is the IBOutlet to the constraint
            self.textContentViewConstraint.constant = height2
            self.view.layoutIfNeeded()
            
        }, completion: {(finished : Bool) in
            if(finished)
            {
                if height == 70 {
                    self.photoBtn.isHidden = true
                    self.videoBtn.isHidden = true
                    self.eventBtn.isHidden = true
                    self.btnStackView.isHidden = true
                    self.postBtn.isHidden = true
                    self.postTypeText.isHidden = true
                }else if height == 230 {
                    self.btnStackView.isHidden = false
                    self.photoBtn.isHidden = false
                    self.videoBtn.isHidden = false
                    self.eventBtn.isHidden = false
                    self.postBtn.isHidden = false
                    self.postTypeText.isHidden = false
                }
            }
        })
    }
    
    @IBAction func eventBtnAction(_ sender: UIButton) {
      //  self.eventView = eventSelectorView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
      //  UIApplication.shared.keyWindow?.addSubview(self.eventView)
      //  //self.view.addSubview(self.eventView)
      //  let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
      //  self.eventView.view.addGestureRecognizer(tap)
      //  self.eventView.view.isUserInteractionEnabled = true
       
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
        if self.eventView != nil {
            self.eventView.removeFromSuperview()
        }
    }
    
    @IBAction func chatAction(_ sender: UIBarButtonItem) {
        
        backView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        backView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        //self.view.addSubview(view)
        UIApplication.shared.keyWindow?.addSubview(backView)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        addController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        addController.delegate = self
        
        addController.view.frame = CGRect(x: UIScreen.main.bounds.width/4 , y: 0, width: UIScreen.main.bounds.width - UIScreen.main.bounds.width/4 , height: UIScreen.main.bounds.height)
        
        let transition = CATransition()
        let withDuration = 1.0
        transition.duration = withDuration
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        addController.view.layer.add(transition, forKey: kCATransition)
        backView.addSubview(addController.view)
        let TapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hideChatView(sender:)))
        self.backView.addGestureRecognizer(TapRecognizer)
    }
    
    @objc func hideChatView(sender:UIGestureRecognizer) {
        self.closeWindow()
    }
    
    func showAttribute(height:CGFloat, height2:CGFloat) {
        UIView.animate(withDuration: 1.0, animations: {
            if height == 70 {
                self.photoBtn.isHidden = true
                self.videoBtn.isHidden = true
                self.eventBtn.isHidden = true
                self.btnStackView.isHidden = true
                self.postBtn.isHidden = true
                self.postTypeText.isHidden = true
            }else if height == 230 {
                self.btnStackView.isHidden = false
                self.photoBtn.isHidden = false
                self.videoBtn.isHidden = false
                self.eventBtn.isHidden = false
                self.postBtn.isHidden = false
                self.postTypeText.isHidden = false
            }
        })
    }
    
    
    
    
    
    @IBAction func postBtnAction(_ sender: DesignButton) {
        self.addPost()
    }
    
    func addPost() {
        
        var visibilty = "0"
       //0=only Me,1=Public,2=friend
        if postTypeText.text! == "Public"{
            visibilty = "1"
        }else if postTypeText.text! == "Friends" {
            visibilty = "2"
        }
        
        if  !Reachability.isConnectedToNetwork() {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.noInterNet, withError: nil, onClose: {
                return
            })
        }
        
        if postType == "image" {
        
        let parameter:[String:String] = [
            "user_id": UserData._id,
            "title": statusText.text!,
            "latitute":"1.2133",
            "logitute":"1.12323",
            "visiblity": visibilty,
            "description":"",
            "type":"1",
            "file_type":"1" // {//0 mean no file,1 means image,2 means vedio}
        ]
        
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            
        let imgData = UIImageJPEGRepresentation(chosenImage!, 0.7)!
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imgData, withName: "picture",fileName: "file.jpg", mimeType: "image/jpg")
            for (key, value) in parameter {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: Defines.ServerUrl + Defines.addPost)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    let activityData = ActivityData()
                    NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    print(response.result.value)
                    let json = JSON(response.result.value)
                    print(json)
                    if json["status"].stringValue == "true" {
                        self.statusText.text = ""
                        self.animation(height: 70, height2: 50)
                        self.view.endEditing(true)
                        self.childOneVC.getPost()
                        self.uploadImage.image = nil
                        self.chosenImage = nil
                        self.uploadImage.isHidden = true
                        self.placeholderLabel.isHidden = false
                        let banner = NotificationBanner(title: "Confirmation", subtitle: json["msg"].stringValue , style: .success)
                        banner.show(queuePosition: .front)
                        
                    }else if json["status"].stringValue == "false" {
                        let banner = NotificationBanner(title: "Alert", subtitle: json["msg"].stringValue , style: .danger)
                        banner.show(queuePosition: .front)
                    }
                }
                
            case .failure(let encodingError):
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                print(encodingError)
            }
        }
        
        }else if self.postType == "text" {
        
            if  !Reachability.isConnectedToNetwork() {
                Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.noInterNet, withError: nil, onClose: {
                    return
                })
            }
            
            let parameter:[String:String] = [
                "user_id": UserData._id,
                "title": statusText.text!,
                "latitute":"1.2133",
                "logitute":"1.12323",
                "visiblity": visibilty,
                "picture":"",
                "description":"",
                "type":"1",
                "file_type":"0" // {//0 mean no file,1 means image,2 means vedio}
            ]
            
            let activityData = ActivityData()
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        DataProvider.sharedInstance.getDataUsingPost(path: Defines.ServerUrl + Defines.addPost, dataDict: parameter, { (json) in
            print(json)
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            if json["status"].stringValue == "true" {
                self.statusText.text = ""
                self.placeholderLabel.isHidden = false
                self.animation(height: 70, height2: 50)
                self.view.endEditing(true)
                self.childOneVC.getPost()
                let banner = NotificationBanner(title: "Confirmation", subtitle: json["msg"].stringValue , style: .success)
                banner.show(queuePosition: .front)
                
            }else if json["status"].stringValue == "false" {
                let banner = NotificationBanner(title: "Alert", subtitle: json["msg"].stringValue , style: .danger)
                banner.show(queuePosition: .front)
            }
            
        }) { (error) in
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            print(error)
        }
       
        }else if self.postType == "video" {
            
            let parameter:[String:String] = [
                "user_id": UserData._id,
                "title": statusText.text!,
                "latitute":"1.2133",
                "logitute":"1.12323",
                "visiblity": visibilty,
                "description":"",
                "type":"1",
                "file_type":"2" // {//0 mean no file,1 means image,2 means vedio}
            ]
            
            let activityData = ActivityData()
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
            
           // let imgData = UIImageJPEGRepresentation(chosenImage!, 0.7)!
            
            Alamofire.upload(multipartFormData: { multipartFormData in
               // multipartFormData.append(imgData, withName: "picture",fileName: "file.jpg", mimeType: "image/jpg")
                multipartFormData.append(self.videoUrl, withName: "picture")
                for (key, value) in parameter {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
            }, to: Defines.ServerUrl + Defines.addPost)
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        let activityData = ActivityData()
                        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                        print(response.result.value)
                        let json = JSON(response.result.value)
                        print(json)
                        if json["status"].stringValue == "true" {
                            self.statusText.text = ""
                            self.animation(height: 70, height2: 50)
                            self.view.endEditing(true)
                            self.childOneVC.getPost()
                            self.placeholderLabel.isHidden = false
                            let banner = NotificationBanner(title: "Confirmation", subtitle: json["msg"].stringValue , style: .success)
                            banner.show(queuePosition: .front)
                            
                        }else if json["status"].stringValue == "false" {
                            let banner = NotificationBanner(title: "Alert", subtitle: json["msg"].stringValue , style: .danger)
                            banner.show(queuePosition: .front)
                        }
                    }
                    
                case .failure(let encodingError):
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    print(encodingError)
                }
            }
            
        }
        
    }
    
    func addEvent(){
        
        var visibilty = "0"
        //0=only Me,1=Public,2=friend
        if postTypeText.text! == "Public"{
            visibilty = "1"
        }else if postTypeText.text! == "Friends" {
            visibilty = "2"
        }
        
        if  !Reachability.isConnectedToNetwork() {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.noInterNet, withError: nil, onClose: {
                return
            })
        }
        
        let parameter = [
            "user_id":UserData._id,
            "title":statusText.text!,
            "description":"",
            "picture":"",
            "event_start_date": self.eventView.startDateText.text!,
            "event_end_date":self.eventView.endDateText.text!,
            "venue":self.eventView.venueText.text!,
            "visibility":visibilty,//{0=only Me,1=Public,2=friend },
            "latitute":"1.2133",
            "logitute":"1.12323",
            "type":"3"
        ]
        
        DataProvider.sharedInstance.getDataUsingPost(path: Defines.ServerUrl + Defines.addEvent, dataDict: parameter, { (json) in
            print(json)
        }) { (error) in
            print(error)
        }
        
    }
    

    @IBAction func searchAction(_ sender: UIBarButtonItem) {
        let storyboard  = UIStoryboard(name: "Main", bundle: nil)
        let nextView = storyboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(nextView, animated: false)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DashboardViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.postTypeText {
            self.dropDown(textField, selector: ["Public","Friends","Only me"])
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension DashboardViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        placeholderLabel.isHidden = true
        
//        self.AnimateBackgroundHeight(height: 230, height2: 147) { (status) in
//            if status == true {
//                     self.showAttribute(height: 230, height2: 147)
//            }
//        }
        
        self.animation(height: 230, height2: 147)
       
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text! == "" {
         placeholderLabel.isHidden = false
//        self.AnimateBackgroundHeight(height: 70, height2: 50) { (status) in
//            if status == true {
//                self.showAttribute(height: 70, height2: 50)
//            }
//        }
            
        self.animation(height: 70, height2: 50)
            
        }
        
        
    }
    
}

extension DashboardViewController : UIPopoverPresentationControllerDelegate,PopViewControllerDelegate {
    
    func dropDown(_ textField:UITextField , selector:[String]) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let popController = storyBoard.instantiateViewController(withIdentifier: "PopViewController") as!  PopViewController
        popController.delegate = self
        popController.arr = selector
        popController.modalPresentationStyle = UIModalPresentationStyle.popover
        popController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.any
        popController.popoverPresentationController?.delegate = self
        popController.popoverPresentationController?.sourceView = textField
        popController.popoverPresentationController?.sourceRect = textField.bounds
        popController.preferredContentSize = CGSize(width: 200, height: 150)
        self.present(popController, animated: true, completion: nil)
        tmpTextField = textField
    }
    
    func saveString(_ strText: String) {
        tmpTextField.text = strText
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle{
        return UIModalPresentationStyle.none
    }
    
}

extension DashboardViewController: chatDelegate {
    
    func closeWindow() {
        addController.view.layer.removeAllAnimations()
        UIView.animate(withDuration: 1.0, animations: {
            self.addController.view.frame.origin.x = 700
            
        }, completion: {(finished : Bool) in
            if(finished)
            {
                self.backView.removeFromSuperview()
                self.addController.willMove(toParentViewController: nil)
                self.addController.view.removeFromSuperview()
                self.addController.removeFromParentViewController()
            }
        })
        
        
    }
}

extension DashboardViewController: UINavigationControllerDelegate , UIImagePickerControllerDelegate {
    
    @IBAction func changePicture(_ sender: Any) {
        let refreshAlert = UIAlertController(title: "Picture", message: title, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Library", style: .default, handler: { (action: UIAlertAction!) in
            self.openLibrary(type: "image")
            
        }))
        refreshAlert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action: UIAlertAction!) in
            self.openCamera(type: "image")
        }))
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in
            
        }))
        present(refreshAlert, animated: true, completion: nil)
        
        
    }
    
    
    @IBAction func takeVideo(_ sender: Any) {
        let refreshAlert = UIAlertController(title: "Picture", message: title, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Library", style: .default, handler: { (action: UIAlertAction!) in
            self.openLibrary(type: "video")
            
        }))
        refreshAlert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action: UIAlertAction!) in
            self.openCamera(type: "video")
        }))
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in
            
        }))
        present(refreshAlert, animated: true, completion: nil)
        
        
    }
    
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let mediaType = info[UIImagePickerControllerMediaType] as? String {
            
            if mediaType  == "public.image" {
                chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
                let image = self.resizeImage(chosenImage, targetSize: CGSize(width: 100.0, height: 100.0))
                uploadImage.isHidden = false
                uploadImage.image = image
                self.postType = "image"
            }
            
            if mediaType == "public.movie" {
                 self.videoUrl = info[UIImagePickerControllerMediaURL] as? URL
               // let image = self.resizeImage(chosenImage, targetSize: CGSize(width: 100.0, height: 100.0))
               // uploadImage.isHidden = false
               // uploadImage.image = chosenImage
                //let url : URL = URL(string: "http://static.videokart.ir/clip/100/480.mp4")!
                self.postType = "video"
                self.videoView.isHidden = false
                player = AVPlayer(url: self.videoUrl!)
                let playerLayer = AVPlayerLayer(player: player)
                playerLayer.frame = self.videoView.bounds
                self.videoView.layer.addSublayer(playerLayer)
            }
        }
        
        dismiss(animated: true, completion: nil)
    }
    /*
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        if let fileURL =  info[UIImagePickerControllerMediaURL] as? URL {
            do {
                try  FileManager.default.moveItem(at: fileURL, to: documentsDirectoryURL.appendingPathComponent("videoName.mov"))
                                                
                    print("movie saved")
            } catch {
                print(error)
            }
        }
    }*/

    
    
    
    // take phota using camera
    func openCamera(type:String) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            if type == "video" {
                imagePicker.mediaTypes = [kUTTypeMovie as String]
            }
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    // take photo from library
    func openLibrary(type:String) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
            print("Button capture")
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            if type == "video" {
                imagePicker.mediaTypes = [kUTTypeMovie as String]
            }
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func resizeImage(_ image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

