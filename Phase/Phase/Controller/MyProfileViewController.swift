//
//  MyProfileViewController.swift
//  Phase
//
//  Created by Mahabir on 15/02/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
import SwiftyJSON
import NotificationBannerSwift
import NVActivityIndicatorView

class MyProfileViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var addController = ChatViewController()
    var backView:UIView!
    var name:String = ""
    var userName:String = ""
    var coverPic:String = ""
    var profilePic:String = ""
    var profession:String = ""
    var dob:String = ""
    var education:String = ""
    var city:String = ""
    var emailAddress:String = ""
    var mobile:String = ""
    var gender:String = ""
    var location:String = ""
    var postArr:[Post] = []
    var postId:String!
    
    var imagePicker = UIImagePickerController()
    var chosenImage:UIImage!
    var profilePicImageView:UIImageView!
    var CoverPicImageView:UIImageView!
    var imagepickerType:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "PROFILE"
        self.tableView.separatorStyle = .none
        
        self.getPost()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getUserProfileData()
    }

    func resizeImage(image:UIImage, targetSize:CGSize) -> UIImage? {
        let originalSize = image.size
        
        let widthRatio = targetSize.width / originalSize.width
        let heightRatio = targetSize.height / originalSize.height
        
        let ratio = min(widthRatio, heightRatio)
        
        let newSize = CGSize(width: originalSize.width * ratio, height: originalSize.height * ratio)
        
        // preparing rect for new image size
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, UIScreen.main.scale)
        image.draw(in: rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    
    
   
    
    
    func getPost(){
        if  !Reachability.isConnectedToNetwork() {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.noInterNet, withError: nil, onClose: {
                return
            })
        }
        
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        DataProvider.sharedInstance.getDataUsingGet(path: Defines.ServerUrl + Defines.getPost + UserData._id, { (json) in
            print(json)
             NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            self.postArr = []
            if json["status"].stringValue == "true" {
                for i in 0..<json["data"].arrayValue.count {
                    self.postArr.append(Post.init(
                        postTitle: json["data"][i]["title"].stringValue,
                        lat: json["data"][i]["latitute"].stringValue,
                        long: json["data"][i]["logitute"].stringValue,
                        _id: json["data"][i]["_id"].stringValue,
                        createdDate: json["data"][i]["created_date"].stringValue,
                        type: json["data"][i]["type"].stringValue,
                        fileType: json["data"][i]["file_type"].stringValue,
                        userName: json["data"][i]["user_id"]["first_name"].stringValue + " " + json["data"][i]["user_id"]["middle_name"].stringValue + " " + json["data"][i]["user_id"]["last_name"].stringValue,
                        userId: json["data"][i]["user_id"][""].stringValue,
                        picturePath: json["path"].stringValue + "/" + json["data"][i]["picture"].stringValue
                    ))
                }
                self.tableView.reloadData()
                print(self.postArr)
            }
        }) { (error) in
            print(error)
             NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        }
    }
    
    
    func getUserProfileData() {
        
        if  !Reachability.isConnectedToNetwork() {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.noInterNet, withError: nil, onClose: {
                return
            })
        }
        
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        DataProvider.sharedInstance.getDataUsingGet(path: Defines.ServerUrl + Defines.getProfile + UserData._id, { (json) in
            print(json)
             NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            self.userName = json["data"]["username"].stringValue
            self.name = json["data"]["first_name"].stringValue
            self.coverPic = json["data"]["cover_picture"].stringValue
            self.profilePic = json["data"]["profile_pic"].stringValue
            self.profession = json["data"]["profession"].stringValue
            self.dob = json["data"]["dob"].stringValue
            self.education = json["data"]["education"].stringValue
            self.city = json["data"]["city"].stringValue
            self.emailAddress = json["data"]["email"].stringValue
            self.mobile = json["data"]["mobile"].stringValue
            self.gender = json["data"]["gender"].stringValue
            self.location = json["data"]["location"].stringValue
            
            self.tableView.reloadData()
            
        }) { (error) in
             NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            print(error)
        }
    }
    
    
    func uploadProfilePic(name:String) {
        
        var url:String = ""
         var imgData:Data!
        
        if name == "profile_pic" {
            url = Defines.ServerUrl + Defines.changeProfilePicture
            imgData = UIImageJPEGRepresentation(profilePicImageView.image!, 0.7)!
        }else{
             url = Defines.ServerUrl + Defines.changeCoverPicture
            imgData = UIImageJPEGRepresentation(CoverPicImageView.image!, 0.7)!
        }
        
            let parameter:[String:String] = [
                "user_id": UserData._id
            ]
            
            let activityData = ActivityData()
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
       
           // let imgData = UIImageJPEGRepresentation(chosenImage!, 0.7)!
        
            print(url)
            print(parameter)
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(imgData, withName: name ,fileName: "file.jpg", mimeType: "image/jpg")
                for (key, value) in parameter {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
            }, to: url)
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        let activityData = ActivityData()
                        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        print(response)
                        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                        print(response.result.value)
                        let json = JSON(response.result.value)
                        print(json)
                        if json["status"].stringValue == "true" {
                            let banner = NotificationBanner(title: "Confirmation", subtitle: json["msg"].stringValue , style: .success)
                            banner.show(queuePosition: .front)
                            
                        }else if json["status"].stringValue == "false" {
                            let banner = NotificationBanner(title: "Alert", subtitle: json["msg"].stringValue , style: .danger)
                            banner.show(queuePosition: .front)
                        }
                    }
                    
                case .failure(let encodingError):
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                    print(encodingError)
                }
            }
            
        
        
    }
    
    @objc func likePostAction(sender:UIButton) {
        let origImage = UIImage(named: "like")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        sender.setImage(tintedImage, for: .normal)
        var status:String = "1"
        if sender.tintColor == .red {
            sender.tintColor = .black
            status = "0"
        }else{
            sender.tintColor = .red
            status = "1"
        }
        
        let parameter:[String:String] = [
            "user_id":UserData._id,
            "post_id":postArr[sender.tag]._id,
            "status":status
        ]
        
        if  !Reachability.isConnectedToNetwork() {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.noInterNet, withError: nil, onClose: {
                return
            })
        }
        
        DataProvider.sharedInstance.getDataUsingPost(path: Defines.ServerUrl + Defines.like, dataDict: parameter, { (json) in
            print(json)
            if json["status"].stringValue == "true" {
                let banner = NotificationBanner(title: "Confirmation", subtitle: json["msg"].stringValue  , style: .success)
                banner.show(queuePosition: .front)
            }else if json["status"].stringValue == "false" {
                let banner = NotificationBanner(title: "Alert", subtitle: json["msg"].stringValue , style: .danger)
                banner.show(queuePosition: .front)
            }
        }) { (error) in
            print(error)
        }
    }
    
    @objc func dropDownAction(sender:UIButton) {
        self.postId = self.postArr[sender.tag]._id
        self.dropDown(sender, selector: ["Edit","Delete"])
    }
    
    func deletePost() {
        
        DataProvider.sharedInstance.getDataUsingGet(path: Defines.ServerUrl + Defines.deletePost + self.postId, { (json) in
            print(json)
            if json["status"].stringValue == "true" {
                let banner = NotificationBanner(title: "Confirmation", subtitle: json["msg"].stringValue  , style: .success)
                banner.show(queuePosition: .front)
                self.deletePost()
            }else if json["status"].stringValue == "false" {
                let banner = NotificationBanner(title: "Alert", subtitle: json["msg"].stringValue , style: .danger)
                banner.show(queuePosition: .front)
            }
            
        }) { (error) in
            print(error)
        }
    }
    
    @IBAction func chatAction(_ sender: UIBarButtonItem) {
        
        backView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        backView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        //self.view.addSubview(view)
        UIApplication.shared.keyWindow?.addSubview(backView)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        addController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        addController.delegate = self
        
        addController.view.frame = CGRect(x: UIScreen.main.bounds.width/4 , y: 0, width: UIScreen.main.bounds.width - UIScreen.main.bounds.width/4 , height: UIScreen.main.bounds.height)
        
        let transition = CATransition()
        let withDuration = 1.0
        transition.duration = withDuration
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        addController.view.layer.add(transition, forKey: kCATransition)
        backView.addSubview(addController.view)
        let TapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hideChatView(sender:)))
        self.backView.addGestureRecognizer(TapRecognizer)
    }
    
    @objc func hideChatView(sender:UIGestureRecognizer) {
        self.closeWindow()
    }
    
    @objc func editDetails(sender:UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let nextView = storyboard.instantiateViewController(withIdentifier: "EditDetailsViewController") as! EditDetailsViewController
        nextView.education = self.education
        nextView.location = self.city
        nextView.work = self.profession
        self.navigationController?.pushViewController(nextView, animated: true)
    }
    
    @objc func describeDetails(sender:UITextField) {
        self.view.endEditing(true)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let nextView = storyboard.instantiateViewController(withIdentifier: "UserDescriptionViewController") as! UserDescriptionViewController
        self.navigationController?.pushViewController(nextView, animated: true)
    }
    
    @objc func galleryAction(sender:UITextField) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let nextView = storyboard.instantiateViewController(withIdentifier: "GalleryViewController") as! GalleryViewController
        nextView.userId = UserData._id
        self.navigationController?.pushViewController(nextView, animated: true)
       
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MyProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postArr.count + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfilePictureCell", for: indexPath) as! ProfilePictureCell
            cell.nameLabel.text = self.name
            self.profilePicImageView = cell.profiePicImage
            self.CoverPicImageView = cell.coverImage
            cell.changeProfilePicBtn.addTarget(self, action: #selector(self.changePicture(_:)), for: UIControlEvents.touchUpInside)
            cell.changeCoverPicBtn.addTarget(self, action: #selector(self.changePictureForCover(_:)), for: UIControlEvents.touchUpInside)
        
        if coverPic != "" {
            let url = URL(string: "http://67.205.173.26:3000/profileImg/" + coverPic)
            let placeholder = UIImage(named: "default")
            let filter = AspectScaledToFillSizeFilter(size: cell.coverImage.frame.size)
            cell.coverImage.af_setImage(withURL: url! , placeholderImage: placeholder, filter: filter)
        }
        
        if profilePic != "" {
            let url = URL(string: "http://67.205.173.26:3000/profileImg/" + profilePic)
            let placeholder = UIImage(named: "default")
            let filter = AspectScaledToFillSizeFilter(size: cell.profiePicImage.frame.size)
            cell.profiePicImage.af_setImage(withURL: url! , placeholderImage: placeholder, filter: filter)
        }
        
            cell.selectionStyle = .none
            return cell
       }/*else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddFriendCell", for: indexPath) as! AddFriendCell
            cell.selectionStyle = .none
            return cell
       }*/
       else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileInfoCell", for: indexPath) as! ProfileInfoCell
        cell.describeText.addTarget(self, action: #selector(self.describeDetails(sender:)), for: UIControlEvents.editingDidBegin)
            cell.educationLabel.text = self.education
            cell.locationLabel.text = self.location
            cell.professionLabel.text = self.profession
        cell.editDetailBtn.addTarget(self, action: #selector(self.editDetails(sender:)), for: UIControlEvents.touchUpInside)
        cell.photos.addTarget(self, action: #selector(self.galleryAction(sender:)), for: UIControlEvents.touchUpInside)
        
            cell.selectionStyle = .none
            return cell
       }/*else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FriendInfoCell", for: indexPath) as! FriendInfoCell
            cell.selectionStyle = .none
            return cell
       }*/else{
            if postArr[indexPath.row - 2].fileType == "0" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "StatusCell", for: indexPath) as! StatusCell
                cell.statusLabel.text = postArr[indexPath.row - 2].postTitle
                cell.userNameLabel.text = postArr[indexPath.row - 2].userName
                cell.likeButton.addTarget(self, action: #selector(self.likePostAction(sender:)), for: UIControlEvents.touchUpInside)
                cell.likeButton.tag = indexPath.row - 2
                cell.dropdownBtn.tag = indexPath.row - 2
                cell.dropdownBtn.addTarget(self, action: #selector(self.dropDownAction(sender:)), for: UIControlEvents.touchUpInside)
                cell.selectionStyle = .none
                return cell
            }else if postArr[indexPath.row - 2].fileType == "1" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "StatusCell2", for: indexPath) as! StatusCell2
                cell.dropDownBtn.tag = indexPath.row - 2
                cell.dropDownBtn.addTarget(self, action: #selector(self.dropDownAction(sender:)), for: UIControlEvents.touchUpInside)
                cell.likeBtn.addTarget(self, action: #selector(self.likePostAction(sender:)), for: UIControlEvents.touchUpInside)
                cell.likeBtn.tag = indexPath.row - 2
                // cell.updateImageView.image = resizeImage(image: #imageLiteral(resourceName: "tom2"), targetSize: cell.updateImageView.frame.size)
                print(Defines.ServerUrl + postArr[indexPath.row - 2].picturePath)
                    // cell.updateImageView.downloadedFrom(url: URL(string: "http://67.205.173.26:3000/" + postArr[indexPath.row].picturePath)!)
                let url = URL(string: "http://67.205.173.26:3000/" + postArr[indexPath.row - 2].picturePath)
                let placeholder = UIImage(named: "default")
            
                let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                size: cell.updateImageView.frame.size,
                radius: 20.0
            )
            
                // let filter = AspectScaledToFillSizeFilter(size: cell.updateImageView.frame.size)
                cell.updateImageView.af_setImage(withURL: url! , placeholderImage: placeholder, filter: filter)
            
                cell.statusLabel.text = postArr[indexPath.row - 2].postTitle
                cell.userNameLabel.text = postArr[indexPath.row - 2].userName
                //cell.updateImageView.image = UIImage(named: "tom2")
                cell.selectionStyle = .none
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "StatusCell3", for: indexPath) as! StatusCell3
                cell.video(url: URL(string: "http://67.205.173.26:3000/" + postArr[indexPath.row - 2].picturePath)!)
                cell.dropDownBtn.tag = indexPath.row - 2
                cell.dropDownBtn.addTarget(self, action: #selector(self.dropDownAction(sender:)), for: UIControlEvents.touchUpInside)
                cell.likeBtn.addTarget(self, action: #selector(self.likePostAction(sender:)), for: UIControlEvents.touchUpInside)
                cell.likeBtn.tag = indexPath.row - 2
                cell.statusLabel.text = postArr[indexPath.row - 2].postTitle
                cell.userNameLabel.text = postArr[indexPath.row - 2].userName
                cell.selectionStyle = .none
                return cell
            }
        }
    }
    
    
    
}




extension MyProfileViewController: chatDelegate {
    
    func closeWindow() {
        addController.view.layer.removeAllAnimations()
        UIView.animate(withDuration: 1.0, animations: {
            self.addController.view.frame.origin.x = 700
            
        }, completion: {(finished : Bool) in
            if(finished)
            {
                self.backView.removeFromSuperview()
                self.addController.willMove(toParentViewController: nil)
                self.addController.view.removeFromSuperview()
                self.addController.removeFromParentViewController()
            }
        })
        
        
    }
}

extension MyProfileViewController : UIPopoverPresentationControllerDelegate,PopViewControllerDelegate {
    
    func dropDown(_ sender:UIButton , selector:[String]) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let popController = storyBoard.instantiateViewController(withIdentifier: "PopViewController") as!  PopViewController
        popController.delegate = self
        popController.arr = selector
        popController.modalPresentationStyle = UIModalPresentationStyle.popover
        popController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
        popController.popoverPresentationController?.delegate = self
        popController.popoverPresentationController?.sourceView = sender
        popController.popoverPresentationController?.sourceRect = sender.bounds
        popController.preferredContentSize = CGSize(width: 100, height: 100)
        self.present(popController, animated: true, completion: nil)
    }
    
    func saveString(_ strText: String) {
        print(strText)
        
        if strText == "Delete" {
            Utility.showMessageDialogWithCancel(onController: self, withTitle: "Alert", withMessage: "Are you sure you want to delete this post?", onClose: {
                self.deletePost()
            })
        }
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle{
        return UIModalPresentationStyle.none
    }
    
}

extension MyProfileViewController: UINavigationControllerDelegate , UIImagePickerControllerDelegate {
    
    @IBAction func changePicture(_ sender: Any) {
        
        self.imagepickerType = "profile"
        let refreshAlert = UIAlertController(title: "Picture", message: title, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Library", style: .default, handler: { (action: UIAlertAction!) in
            self.openLibrary(type: "image")
            
        }))
        refreshAlert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action: UIAlertAction!) in
            self.openCamera(type: "image")
        }))
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in
            
        }))
        present(refreshAlert, animated: true, completion: nil)
        
        
    }
    
    @IBAction func changePictureForCover(_ sender: Any) {
        
        self.imagepickerType = "cover"
        let refreshAlert = UIAlertController(title: "Picture", message: title, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Library", style: .default, handler: { (action: UIAlertAction!) in
            self.openLibrary(type: "image")
            
        }))
        refreshAlert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action: UIAlertAction!) in
            self.openCamera(type: "image")
        }))
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in
            
        }))
        present(refreshAlert, animated: true, completion: nil)
        
        
    }

    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let mediaType = info[UIImagePickerControllerMediaType] as? String {
            
            if mediaType  == "public.image" {
                chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
                
               // uploadImage.isHidden = false
                if self.imagepickerType == "profile" {
                    let image = self.resizeImage(chosenImage, targetSize: CGSize(width: profilePicImageView.frame.size.width, height: profilePicImageView.frame.size.height))
                    profilePicImageView.image = image
                    self.uploadProfilePic(name:"profile_pic")
                }else{
                    let image = self.resizeImage(chosenImage, targetSize: CGSize(width: CoverPicImageView.frame.size.width, height: CoverPicImageView.frame.size.height))
                    CoverPicImageView.image = image
                    self.uploadProfilePic(name:"cover_picture")
                }
            }
        }
        
        dismiss(animated: true, completion: nil)
    }
    /*
     
     func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
     let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
     if let fileURL =  info[UIImagePickerControllerMediaURL] as? URL {
     do {
     try  FileManager.default.moveItem(at: fileURL, to: documentsDirectoryURL.appendingPathComponent("videoName.mov"))
     
     print("movie saved")
     } catch {
     print(error)
     }
     }
     }*/
    
    
    
    
    // take phota using camera
    func openCamera(type:String) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    // take photo from library
    func openLibrary(type:String) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
            print("Button capture")
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func resizeImage(_ image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

