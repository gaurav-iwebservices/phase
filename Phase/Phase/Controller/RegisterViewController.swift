//
//  RegisterViewController.swift
//  Phase
//
//  Created by Mahabir on 15/02/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit

import NotificationBannerSwift
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class RegisterViewController: UIViewController {

    @IBOutlet weak var cityText: UITextField!
    @IBOutlet weak var countryText: UITextField!
    @IBOutlet weak var phoneText: UITextField!
    @IBOutlet weak var genderText: UITextField!
    @IBOutlet weak var dateBirthText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    
    @IBOutlet weak var userNameText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var lastNameText: UITextField!
    
    var pickerView:PickerTool!
    var userImage: UIImage!
    var imagePicker = UIImagePickerController()
    
    var countryName:[String] = []
    var countryId:[String] = []
    var isAgree = false
    override func viewDidLoad() {
        super.viewDidLoad()

        backgroundView.backgroundColor = UIColor.clear
        dateBirthText.addTarget(self, action: #selector(self.datePicker(_:)), for: UIControlEvents.editingDidBegin)
       
        
        countryText.delegate = self
        lastNameText.delegate = self
        cityText.delegate = self
        phoneText.delegate = self
        genderText.delegate = self
        dateBirthText.delegate = self
        passwordText.delegate = self
        //profilePictureText.delegate = self
        userNameText.delegate = self
        emailText.delegate = self
        nameText.delegate = self
        confirmPassword.delegate = self
        self.setImageInTextField(textField: countryText)
        self.setImageInTextField(textField: genderText)
        self.setImageInTextField(textField: dateBirthText)
        
        self.getCountryList()
        
        pickerView = PickerTool.loadClass() as? PickerTool
        countryText.inputView = pickerView
        genderText.inputView = pickerView
        
        /*pickerView = PickerTool.loadClass() as? PickerTool
        countryTextField?.inputView = pickerView;
        sectorTextField?.inputView = pickerView;
        ageTextField?.inputView = pickerView;*/
    
        
        let doubleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard(sender:)))
        self.view.addGestureRecognizer(doubleTapRecognizer)
    }
    @objc func hideKeyboard(sender:UIGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func  setImageInTextField(textField:UITextField) {
        textField.rightViewMode = UITextFieldViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        
        if textField == dateBirthText {
            imageView.image = UIImage(named: "calender")
        }else{
            imageView.image = UIImage(named: "drop_down")
        }
        textField.rightView = imageView
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func signInAction(_ sender: UIButton) {
        var isExist = false
        if let viewControllers = self.navigationController?.viewControllers {
            for viewController in viewControllers {
                if viewController.isKind(of: LoginViewController.self) {
                    isExist = true
                    self.navigationController?.popToViewController(viewController, animated: true)
                }
            }
        }
        
        if isExist == false {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let nextView = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(nextView, animated: true)
        }
    }
    
    @IBAction func termsAction(_ sender: UIButton) {
        if isAgree == false {
            isAgree = true
            sender.setImage(#imageLiteral(resourceName: "checkbox"), for: UIControlState.normal)
        }else{
            isAgree = false
            sender.setImage(#imageLiteral(resourceName: "uncheckbox"), for: UIControlState.normal)
        }
    }
    
    @IBAction func datePicker(_ sender: UITextField) {
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(handleDatePickerforDOB(sender:)), for: .valueChanged)
    }
    
    @objc func handleDatePickerforDOB(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateBirthText.text = dateFormatter.string(from: sender.date)
    }
    
    
    
   
    
    func getCountryList(){
        
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        DataProvider.sharedInstance.getDataUsingGet(path: Defines.ServerUrl + Defines.countryList, { (json) in
            print(json)
           
            if json["status"].stringValue == "true" {
                for i in 0..<json["list"].arrayValue.count {
                    self.countryName.append(json["list"][i]["country_name"].stringValue)
                    self.countryId.append(json["list"][i]["_id"].stringValue)
                }
            }
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            print("hello")
        }) { (error) in
            print(error)
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        }
    }
    
    @IBAction func registerAction(_ sender: DesignButton) {
        if nameText.text == "" {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: "Name field should not blank.", withError: nil, onClose: {
                self.nameText.becomeFirstResponder()
            })
            return
        }
        
        if emailText.text == "" {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: "Email field should not blank.", withError: nil, onClose: {
                self.emailText.becomeFirstResponder()
            })
            return
        }
        
        if userNameText.text == "" {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: "User Name field should not blank.", withError: nil, onClose: {
                self.userNameText.becomeFirstResponder()
            })
            return
        }
        
        if passwordText.text == "" {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: "Password field should not blank.", withError: nil, onClose: {
                self.passwordText.becomeFirstResponder()
            })
            return
        }
        
        if passwordText.text! != confirmPassword.text! {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: "Password and confirm password does not match.", withError: nil, onClose: {
                self.confirmPassword.becomeFirstResponder()
            })
            return
        }
        
        if dateBirthText.text == "" {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: "Date of Birth field should not blank.", withError: nil, onClose: {
                self.dateBirthText.becomeFirstResponder()
            })
            return
        }
        
        if genderText.text == "" {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: "Gender field should not blank.", withError: nil, onClose: {
                self.genderText.becomeFirstResponder()
            })
            return
        }
        
        if phoneText.text == "" {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: "Phone field should not blank.", withError: nil, onClose: {
                self.phoneText.becomeFirstResponder()
            })
            return
        }
        
        if countryText.text == "" {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: "Country field should not blank.", withError: nil, onClose: {
                self.countryText.becomeFirstResponder()
            })
            return
        }
        
        if isAgree == false {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: "Please accept Terms and Conditions.", withError: nil, onClose: {
            })
            return
        }
        
        if  !Reachability.isConnectedToNetwork() {
            Utility.showMessageDialog(onController: self, withTitle: Defines.alterTitle, withMessage: Defines.noInterNet, withError: nil, onClose: {
                return
            })
        }
        
       // "first_name,email,profile_pic,dob,
        //,gender,mobile ,address1,address2(optional),country,state(optional),city,zipcode"
        //"username,password,first_name,email,profile_pic,dob,
      //  ,gender,mobile ,address1,address2(optional),country,state(optional),city"
        
        let index = countryName.index(of: countryText.text!)
        
        
        let parameter:[String:String] = [
            "username": userNameText.text!,
            "password": passwordText.text!,
            "first_name":nameText.text!,
            "last_name" : lastNameText.text!,
            "email":emailText.text!,
            "dob":dateBirthText.text!,
            "gender":genderText.text!,
            "mobile":phoneText.text!,
            "address1": " " ,
            "address2": " ",
            "country":countryId[index!],
            "state":"",
            "city":cityText.text!
        ]
        
        print(parameter)/*{
         "status" : true,
         "data" : {
         "state" : "",
         "updated_date" : "2018-02-22T12:28:54.591Z",
         "address1" : "bxhhhshj",
         "dob" : "22\/02\/2008",
         "chatPassword" : "12345678",
         "last_name" : "",
         "address2" : "bxhbsj",
         "change_password_status" : 0,
         "create_date" : "2018-02-22T12:28:54.591Z",
         "password" : "$2a$10$8uRT6y2JDntApZqvpSyMhePcmcxZE74m3O97orwxygZxWCDny\/9Bq",
         "verify_email_code" : "EVKS",
         "verify_status_by_phone" : 0,
         "city" : "avgvfgvh",
         "profile_pic" : "\/var\/www\/html\/project\/social\/uploads\/profileImg\/profile_img-1519302533980.jpg",
         "email" : "gaurav12@gmail.com",
         "mobile" : 7409655911,
         "gender" : "Male",
         "username" : "gaurav12",
         "verify_phone_code" : 566,
         "status" : 0,
         "chatUserName" : "gaurav12",
         "_id" : "5a8eb7867f9f674803f57e8f",
         "verify_status_by_email" : 1,
         "first_name" : "Gaurav",
         "__v" : 0,
         "middle_name" : "",
         "country" : "5a71b60288ebf3b903ec3e38"
         },
         "msg" : "successful created new user."
         }*/
        
        
        let activityData = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        DataProvider.sharedInstance.getDataUsingPost(path: Defines.ServerUrl + Defines.register, dataDict: parameter, { (json) in
            
            print(json)
            
            if json["status"].stringValue == "true" {
                let banner = NotificationBanner(title: "Confirmation", subtitle: json["msg"].stringValue , style: .success)
                banner.show(queuePosition: .front)
                
                UserData._id = json["data"]["_id"].stringValue
                UserData.city = json["data"]["city"].stringValue
                UserData.country = json["data"]["country"].stringValue
                UserData.dob = json["data"]["dob"].stringValue
                UserData.email = json["data"]["email"].stringValue
                UserData.gender = json["data"]["gender"].stringValue
                UserData.mobile = json["data"]["mobile"].stringValue
                UserData.name = json["data"]["first_name"].stringValue
                UserData.userName = json["data"]["username"].stringValue
               // UserData.userToken = json["token"].stringValue
                
                UserDefaults.standard.setValue(UserData.name, forKey: "firstName")
                UserDefaults.standard.setValue(UserData.email, forKey: "email")
                UserDefaults.standard.setValue(UserData._id, forKey: "Id")
                UserDefaults.standard.setValue(UserData.city, forKey: "city")
                UserDefaults.standard.setValue(UserData.country, forKey: "country")
                UserDefaults.standard.setValue(UserData.dob, forKey: "dob")
                UserDefaults.standard.setValue(UserData.userName, forKey: "userName")
                UserDefaults.standard.setValue(UserData.gender, forKey: "gender")
                UserDefaults.standard.setValue(UserData.mobile, forKey: "mobile")
               // UserDefaults.standard.setValue(UserData.userToken, forKey: "token")
                
                
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let nextView = storyboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                nextView.userId = json["data"]["_id"].stringValue
                self.navigationController?.pushViewController(nextView, animated: true)
                
            }else if json["status"].stringValue == "false" {
                let banner = NotificationBanner(title: "Alert", subtitle: json["msg"].stringValue , style: .danger)
                banner.show(queuePosition: .front)
            }
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
        }) { (error) in
            print(error)
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        }
        
       /* let imgData = UIImageJPEGRepresentation(userImage!, 0.7)!
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imgData, withName: "profile_img",fileName: "file.jpg", mimeType: "image/jpg")
            for (key, value) in parameter {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: Defines.ServerUrl + Defines.register)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    print(response.result.value)
                    let json = JSON(response.result.value)
                    print(json)
                    if json["status"].stringValue == "true" {
                        let banner = NotificationBanner(title: "Confirmation", subtitle: json["msg"].stringValue , style: .success)
                        banner.show(queuePosition: .front)
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let nextView = storyboard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                        nextView.userId = json["data"]["_id"].stringValue
                        self.navigationController?.pushViewController(nextView, animated: true)
                        
                    }else if json["status"].stringValue == "false" {
                        let banner = NotificationBanner(title: "Alert", subtitle: json["msg"].stringValue , style: .danger)
                        banner.show(queuePosition: .front)
                    }
                }
                
            case .failure(let encodingError):
                print(encodingError)
            }
        }*/
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RegisterViewController:UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if nameText == textField {
            let allowedCharacter = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
            let aSet = CharacterSet(charactersIn:allowedCharacter).inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
            
        }else if nameText == textField {
            let allowedCharacter = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
            let aSet = CharacterSet(charactersIn:allowedCharacter).inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
            
        }else if emailText == textField {
            let allowedCharacter = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.@"
            let aSet = CharacterSet(charactersIn:allowedCharacter).inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }else if userNameText == textField {
            let allowedCharacter = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz."
            let aSet = CharacterSet(charactersIn:allowedCharacter).inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }else if dateBirthText == textField {
            let allowedCharacter = "0123456789/-"
            let aSet = CharacterSet(charactersIn:allowedCharacter).inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }else if genderText == textField {
            let allowedCharacter = "FEMALEfemale"
            let aSet = CharacterSet(charactersIn:allowedCharacter).inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }else if phoneText == textField {
            let allowedCharacter = "0123456789+- "
            let aSet = CharacterSet(charactersIn:allowedCharacter).inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }else if countryText == textField {
            let allowedCharacter = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
            let aSet = CharacterSet(charactersIn:allowedCharacter).inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }else if cityText == textField {
            let allowedCharacter = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
            let aSet = CharacterSet(charactersIn:allowedCharacter).inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }
        
        
       /* let allowedCharacter = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz?.,/ "
        
        let aSet = CharacterSet(charactersIn:allowedCharacter).inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        if string == numberFiltered{
            let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let numberOfChars = newText.count
            return numberOfChars < 51
            
        }else{
            return string == numberFiltered
        }*/
        
       return true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if textField == countryText{
            print(countryName)
            if countryName.count > 0 {
                self.setPickerInfo( countryText, withArray: countryName)
            }
        }else if textField == genderText {
            genderText.text! = "Male"
            self.setPickerInfo(genderText, withArray: ["Male","Female"])
        }
        
    }
    
    func setPickerInfo(_ textfield: UITextField, withArray array: [Any])
    {
        //let weakPatientInfo = newPaitent
        pickerView?.pickerViewMethod(textfield, arr: array as! [AnyHashable])
        /*pickerView?.completionHandler = {(_ detail: PickerModel) -> Void in
            if textfield == self.countryTextField
            {
                self.countryId = detail.dataId
            }
            textfield.resignFirstResponder()
        }*/
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == nameText {
            if textField.text!  == "" {
                
            }
        }else if textField == emailText {
            if !Utility.isValidEmail(email: textField.text!) {
                let banner = NotificationBanner(title: "Alert", subtitle: Defines.invalidEmail , style: .danger)
                banner.show(queuePosition: .front)
            }
        }else if textField == userNameText {
            if textField.text!  == "" {
                let banner = NotificationBanner(title: "Alert", subtitle: "Username should not be blank" , style: .danger)
                banner.show(queuePosition: .front)
            }
        }else if textField == passwordText {
            if textField.text!  == "" {
                let banner = NotificationBanner(title: "Alert", subtitle: "Password should not be blank" , style: .danger)
                banner.show(queuePosition: .front)
            }
        }else if textField == dateBirthText {
            if textField.text!  == "" {
                let banner = NotificationBanner(title: "Alert", subtitle: "Date of birth should not be blank" , style: .danger)
                banner.show(queuePosition: .front)
            }
        }else if textField == genderText {
            if textField.text!  == "" {
                let banner = NotificationBanner(title: "Alert", subtitle: "Gender should not be blank" , style: .danger)
                banner.show(queuePosition: .front)
            }
        }else if textField == phoneText {
            if textField.text!  == "" {
                let banner = NotificationBanner(title: "Alert", subtitle: "Phone should not be blank" , style: .danger)
                banner.show(queuePosition: .front)
            }
        }else if textField == countryText {
            if textField.text!  == "" {
                let banner = NotificationBanner(title: "Alert", subtitle: "Country should not be blank" , style: .danger)
                banner.show(queuePosition: .front)
            }
        }
        
    }
}

/*extension RegisterViewController: UINavigationControllerDelegate , UIImagePickerControllerDelegate {
    
    @IBAction func changePicture(_ sender: Any) {
        let refreshAlert = UIAlertController(title: "Picture", message: title, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Library", style: .default, handler: { (action: UIAlertAction!) in
            self.openLibrary()
            
        }))
        refreshAlert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action: UIAlertAction!) in
            self.openCamera()
        }))
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in
            
        }))
        present(refreshAlert, animated: true, completion: nil)
        
        
    }
    
    
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        userImage = self.resizeImage(chosenImage, targetSize: CGSize(width: 300.0, height: 300.0))
        dismiss(animated: true, completion: nil)
    }
    
    
    
    // take phota using camera
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    // take photo from library
    func openLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
            print("Button capture")
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    func resizeImage(_ image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}*/

