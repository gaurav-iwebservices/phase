//
//  AppDelegate.swift
//  Phase
//
//  Created by Mahabir on 15/02/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FBSDKCoreKit
import TwitterKit
import GoogleSignIn


//facebook, twitter, gmail login
//jacobwilson8800@gmail.com
//password - iws1234#

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
         UIApplication.shared.statusBarStyle = .lightContent
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        IQKeyboardManager.sharedManager().shouldShowTextFieldPlaceholder = true
        
        UINavigationBar.appearance().barTintColor = UIColor(red: 153.0/255.0, green: 204.0/255.0, blue: 253.0/255.0, alpha: 1.0)
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
       // UITabBar.appearance().backgroundColor = UIColor.black
      //UITabBar.appearance().tintColor = UIColor(red: 138.0/255.0, green: 138.0/255.0, blue: 138.0/255.0, alpha: 1.0)
        UITabBar.appearance().tintColor = UIColor.black
        //931421253781-nc06pj0kdulka15870909ovaqplm98tu.apps.googleusercontent.com
        
        //Twitter Login
        TWTRTwitter.sharedInstance().start(withConsumerKey:"mnShAYYEYMlmY4lzr5esbfJF0", consumerSecret:"JGdYIPPtSe9EWSvWqDpUlNsxdHvSyXnbYw47bzHUYYsAiSFEDL")
        
        //Facebook Login
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        
        // Initialize Google sign-in
      //  var configureError: NSError?
        //GGLContext.sharedInstance().configureWithError(&configureError)
        //assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
        //GIDSignIn.sharedInstance().delegate = self
        
        //Google Login
        GIDSignIn.sharedInstance().clientID = "931421253781-nc06pj0kdulka15870909ovaqplm98tu.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        
        return true
    }
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        var signedIn: Bool = GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication, annotation: annotation)
        
        signedIn = signedIn ? signedIn : FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        
        return signedIn
    }
    
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        
        var signedIn: Bool = GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        
        signedIn = signedIn ? signedIn : FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: nil)
        
        return signedIn
    }
   
/*
    //MARK:- Facebook Login Delegate
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        let handled: Bool = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        // Add any custom logic here.
        return handled
    }
    
    //MARK:- Google Login Delegate
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        
       // return TWTRTwitter.sharedInstance().application(app, open: url, options: options)
    }*/
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name!
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email!
            // ...
            print(fullName, givenName, familyName, email)
            
            
            let dataDict:[String:String] =  [
                "first_name": givenName!,
                "last_name": familyName!,
                "email": email,
                "username": givenName!,
                "socialkey":"3", // (1=>facebook,2=>twitter ,3=>gmail )
                "socialid": userId!
            ]
          //  LoginViewController().socialAction(data: dataDict)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "googleSignIn"), object: nil, userInfo: dataDict)
            //NotificationCenter.defaultCenter.postNotificationName("googleSignIn", object: nil, userInfo: dataDict)
            
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

