//
//  eventSelectorView.swift
//  Phase
//
//  Created by Mahabir on 27/02/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit

class eventSelectorView: UIView {

    
    @IBOutlet weak var venueText: UITextField!
    @IBOutlet weak var endDateText: UITextField!
    @IBOutlet weak var startDateText: UITextField!
    @IBOutlet weak var subView: UIView!
    var view:UIView!
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "eventSelectorView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        subView.layer.cornerRadius = 5
        endDateText.addTarget(self, action: #selector(self.datePicker(_:)), for: UIControlEvents.editingDidBegin)
        startDateText.addTarget(self, action: #selector(self.datePicker(_:)), for: UIControlEvents.editingDidBegin)
        return view
    }
    
    func  setImageInTextField(textField:UITextField) {
        textField.rightViewMode = UITextFieldViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        imageView.image = UIImage(named: "dropdown")
        textField.rightView = imageView
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(view)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        xibSetup()
    }
    
    @IBAction func datePicker(_ sender: UITextField) {
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .dateAndTime
        sender.inputView = datePickerView
        if sender == endDateText {
            datePickerView.addTarget(self, action: #selector(handleDatePickerforEndDate(sender:)), for: .valueChanged)
        }else{
            datePickerView.addTarget(self, action: #selector(handleDatePickerforStartDate(sender:)), for: .valueChanged)
        }
    }
    
    @objc func handleDatePickerforEndDate(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm"
        endDateText.text = dateFormatter.string(from: sender.date)
    }
    
    @objc func handleDatePickerforStartDate(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm"
        startDateText.text = dateFormatter.string(from: sender.date)
    }

}
