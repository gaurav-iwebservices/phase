//
//  ChatView.swift
//  Phase
//
//  Created by Mahabir on 04/04/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import UIKit

class ChatView: UIView {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var swipeGesture: UISwipeGestureRecognizer!
    var view:UIView!
    
    var friendsArray:[String] = ["Jhon doe","Marry","Peter"]
    var sectionArr:[String] = ["AROUND","FRIENDS","GROUPS"]
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "ChatView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
       // view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        return view
    }
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(view)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        xibSetup()
    }

}


extension ChatView:UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friendsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       // let cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell", for: indexPath) as! ChatCell
        let cell = UITableViewCell()
        cell.textLabel?.text = friendsArray[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    /*func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatHeaderCell") as! ChatHeaderCell
        cell.headerLabel.text = sectionArr[section]
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 44
        
    }*/
    
}
