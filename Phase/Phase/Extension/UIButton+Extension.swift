//
//  UIButton+Extension.swift
//  Phase
//
//  Created by Mahabir on 15/02/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
open class DesignButton: UIButton {
    
    @IBInspectable var conerRadius:CGFloat = 0.0 {
        didSet{
            layer.cornerRadius = conerRadius
        }
        
    }
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}

