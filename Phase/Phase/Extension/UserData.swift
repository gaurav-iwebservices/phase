//
//  UserData.swift
//  Phase
//
//  Created by Mahabir on 26/02/18.
//  Copyright © 2018 Mahabir. All rights reserved.
//

import Foundation

class UserData: NSObject {
    
    static var userToken:String = ""
    static var name:String = ""
    static var _id:String = ""
    static var userName:String = ""
    static var gender:String = ""
    static var mobile:String = ""
    static var email:String = ""
    static var city:String = ""
    static var dob:String = ""
    static var country:String = ""
}
